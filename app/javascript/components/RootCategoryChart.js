import React from "react";
import PropTypes from "prop-types";
import {
  XYPlot,
  XAxis,
  YAxis,
  VerticalGridLines,
  HorizontalGridLines,
  VerticalBarSeries,
  DiscreteColorLegend,
} from 'react-vis';

class RootCategoryChart extends React.Component {
  render () {
    const data = this.props.data;
    const columns = this.props.columns;
    let complete = [];
    let incomplete = [];
    let ongoing = [];
    let unknown = [];
    data.map((item) => {
      if (item.status == 'complete') {
        complete.push({'x':item.name, 'y':item.status_count})
      } 
      if (item.status == 'incomplete') {
        incomplete.push({'x':item.name, 'y':item.status_count})
      }
      if (item.status == 'ongoing') {
        ongoing.push({'x':item.name, 'y':item.status_count})
      }
      if (item.status == 'unknown') {
        unknown.push({'x':item.name, 'y':item.status_count})
      }
    }
      
    );

    return (
      <div className='container-fluid'>
        <div className='row'>
          <div className='col'>
            <XYPlot
              margin={{top: 30, bottom: 150}}
              xType="ordinal"
              width={1550}
              height={550}
            >
              <DiscreteColorLegend
                style={{position: 'absolute', left: '1580px', top: '10px'}}
                orientation="horizontal"
                items={[
                  {
                    title: columns.audit_reports_count,
                    color: '#12939A'
                  },
                  {
                    title: columns.recommendations_count,
                    color: '#79C7E3'
                  }
                ]}
              />
              <VerticalGridLines />
              <HorizontalGridLines />
              <XAxis tickLabelAngle={-45} />
              <YAxis />
              <VerticalBarSeries
                cluster={columns.audit_reports_count}
                color="#12939A"
                data={ars}
              />
              <VerticalBarSeries
                cluster={columns.recommendations_count}
                color="#79C7E3"
                data={recs}
              />
            </XYPlot>
          </div>
        </div>
        <div className='row'>
          <div className='col'>
            <XYPlot
              margin={{left: 100, top: 30, bottom: 150}}
              xType="ordinal"
              width={1550}
              height={550}
            >
              <DiscreteColorLegend
                style={{position: 'absolute', left: '1580px', top: '10px'}}
                orientation="horizontal"
                items={[
                  {
                    title: columns.total_cost,
                    color: '#12939A'
                  },
                  {
                    title: columns.total_recovered,
                    color: '#79C7E3'
                  }
                ]}
              />
              <VerticalGridLines />
              <HorizontalGridLines />
              <XAxis tickLabelAngle={-45} />
              <YAxis />
              <VerticalBarSeries
                cluster={columns.total_cost}
                color="#12939A"
                data={cost}
              />
              <VerticalBarSeries
                cluster={columns.total_recovered}
                color="#79C7E3"
                data={recovered}
              />
            </XYPlot> 
          </div>
        </div>
      </div>
        
    );
  }
}

RootCategoryChart.propTypes = {
  data: PropTypes.array,
  columns: PropTypes.object
};
export default RootCategoryChart
