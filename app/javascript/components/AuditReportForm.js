import React from "react"
import PropTypes from "prop-types"
import axios from 'axios'
import SelectUser from './selects/SelectUser.js';
import SelectYear from './selects/SelectYear.js';
import SelectSensitivity from './selects/SelectSensitivity.js';
import DataSelect from './selects/DataSelect.js'
class AuditReportForm extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.auditReport.id || "",
      title: this.props.auditReport.title || "",
      recipient: this.props.auditReport.recipient || "",
      text: this.props.auditReport.text || "",
      year: this.props.auditReport.year || new Date().getFullYear(),
      period: this.props.auditReport.period || "",
      audit_type_id: this.props.auditReport.audit_type_id || "",
      control_organization_id: this.props.auditReport.control_organization_id || "",
      verified_organization_id: this.props.auditReport.verified_organization_id || "",
      arrival_date: this.props.auditReport.arrival_date || "",
      assignment_date: this.props.auditReport.assignment_date  || "",
      received_date: this.props.auditReport.received_date  || "",
      confirmation_date: this.props.auditReport.confirmation_date  || "",
      user_id: this.props.auditReport.user_id || "",
      sensitivity: this.props.auditReport.sensitivity || "",
      addOrganization: false,
      emptyTitle: false,
      alertMessage: "",
      ministry_id: "",
      region_id: "",
      organization_abbreviation: "",
      organization_name: "",
    };

    this.handleAlertMessage = this.handleAlertMessage.bind(this);
    this.handleTitleBlur = this.handleTitleBlur.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleArrivalDate = this.handleArrivalDate.bind(this);
    this.handleAssignmentDate = this.handleAssignmentDate.bind(this);
    this.handleReceivedDate = this.handleReceivedDate.bind(this);
    this.handleConfirmationDate = this.handleConfirmationDate.bind(this);
    this.handleOrgAdd = this.handleOrgAdd.bind(this);
    this.verifiedOrganizations = this.verifiedOrganizations.bind(this);
    this.organizationDisplay = this.organizationDisplay.bind(this);
    this.organizationToggle = this.organizationToggle.bind(this);
    this.addOrganization = this.addOrganization.bind(this);
  }

  handleArrivalDate() {
    this.setState({
      arrival_date: document.getElementById('arrival_date_input').value
    }); 
  }

  handleAssignmentDate() {
    this.setState({
      assignment_date: document.getElementById('assignment_date_input').value
    }); 
  }

  handleReceivedDate() {
    this.setState({
      received_date: document.getElementById('received_date_input').value
    }); 
  }

  handleConfirmationDate() {
    this.setState({
      confirmation_date: document.getElementById('confirmation_date_input').value
    }); 
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  handleTitleBlur(event) {
    const target = event.target;
    const value = target.value;
    if (value.length < 1) {
      this.setState({emptyTitle: true});
    } else {
      this.setState({emptyTitle: false});
    }
    
  }

  handleSubmit(event) {
    event.preventDefault();

    const audit_report = {
      title: this.state.title,
      recipient: this.state.recipient,
      text: this.state.text,
      year: this.state.year,
      period: this.state.period,
      audit_type_id: this.state.audit_type_id || document.getElementById('audit_type_id').value,
      control_organization_id: this.state.control_organization_id || document.getElementById('control_organization_id').value,
      verified_organization_id: this.state.verified_organization_id || document.getElementById('verified_organization_id').value,
      arrival_date: this.state.arrival_date,
      assignment_date: this.state.assignment_date,
      received_date: this.state.received_date,
      confirmation_date: this.state.confirmation_date,
      user_id: this.state.user_id || document.getElementById('user_id').value,
      sensitivity: this.state.sensitivity || document.getElementById('sensitivity').value,
    };

    axios.defaults.headers.common['X-CSRF-TOKEN'] = this.props.authenticityToken;
    if (this.state.title) {
      if (this.state.id) {
        axios.put(`/audit_reports/${this.state.id}`, { audit_report })
        .then(res => {
          this.setState({alertMessage: this.props.labels["save_success"]});
        })
        .catch(error => {
          this.setState({alertMessage: this.props.labels["save_fail"]});
        }) 
      } else {
        axios.post(`/audit_reports/`, { audit_report })
        .then(res => {
          this.setState({id: res.data.id});
          this.setState({alertMessage: this.props.labels["save_success"]});
        })
        .catch(error => {
          console.log(error.response.data);
          this.setState({alertMessage: this.props.labels["save_fail"]});
        }) 
      }
    }
    else {
      this.setState({alertMessage: this.props.labels["required_fields"]});
    }
  }

  handleOrgAdd(event) {
    event.preventDefault();

    const organization = {
      abbreviation: this.state.organization_abbreviation,
      name: this.state.organization_name,
      region_id: this.state.region_id  || document.getElementById('region_id').value,
      ministry_id: this.state.ministry_id || document.getElementById('ministry_id').value,
      control: false,
    };

    axios.defaults.headers.common['X-CSRF-TOKEN'] = this.props.authenticityToken;

    if (this.state.organization_abbreviation && this.state.organization_name) {
       axios.post(`/organizations.json`, { organization })
        .then(res => {
          this.setState({verified_organization_id: String(res.data.id)});
          this.setState({alertMessage: this.props.labels["save_success"]});
          this.setState({addOrganization: false});
        })
        .catch(error => {
          console.log(error.response.data);
          this.setState({alertMessage: this.props.labels["save_fail"]});
        }) 
    }
    else {
      this.setState({alertMessage: this.props.labels["required_fields"]});
    }
  }

  organizationDisplay() {
    if (!this.state.addOrganization) {
      return this.verifiedOrganizations();
    } else {
      return this.addOrganization() 
    }
  }

  organizationToggle() {
    this.setState({addOrganization: !this.state.addOrganization});
  }

  addOrganization() {
    return (
      <div className="form-row">
        <div className="form-group col-md-2">
          <label>{this.props.labels["organization_abbreviation"]}</label><span className="text-danger">*</span>
          <input
              name="organization_abbreviation"
              type="text"
              value={this.state.organization_abbreviation}
              onChange={this.handleInputChange}
              className="form-control" />
        </div>
        <div className="form-group col-md-3">
          <label>{this.props.labels["organization_name"]}</label><span className="text-danger">*</span>
          <input
              name="organization_name"
              type="text"
              value={this.state.organization_name}
              onChange={this.handleInputChange}
              className="form-control" />
        </div>
        <div className="form-group col-md-3">
          <label>{this.props.labels["ministry"]}</label><span className="text-danger">*</span>
          <DataSelect
            selected={String(this.state.ministry_id)}
            onValueChange={this.handleInputChange}
            selectName="ministry_id"
            dataUrl="ministries.json"  />
        </div>
        <div className="form-group col-md-3">
          <label>{this.props.labels["region"]}</label><span className="text-danger">*</span>
          <DataSelect
            selected={String(this.state.region_id)}
            onValueChange={this.handleInputChange}
            selectName="region_id"
            dataUrl="regions.json"  />
        </div>
        <div className="form-group col-md-1">
          <label>&nbsp;</label>
          <div>
            <button className="btn btn-success" onClick={this.handleOrgAdd}><i className="fa fa-plus"></i></button> &nbsp;
            <button className="btn btn-danger" onClick={this.organizationToggle}><i className="fa fa-times"></i></button>
          </div>
        </div>
      </div>
    )
  }

  verifiedOrganizations() {
    return (
      <div className="form-row">
        <div className="form-group col-md-11">
          <label>{this.props.labels["verified_organization"]}</label>
          <DataSelect
              selected={String(this.state.verified_organization_id)}
              onValueChange={this.handleInputChange}
              selectName="verified_organization_id"
              dataUrl="organizations.json?type=verified" />
        </div>
        <div className="form-group col-md-1">
          <label>&nbsp;</label>
          <button className='btn btn-primary form-control' onClick={this.organizationToggle}>
            {this.props.labels["add"]}
          </button>
        </div>
      </div>
    )
  }

  handleAlertMessage() {
    if (this.state.alertMessage) {
      return (
        <div className="alert alert-info alert-dismissible fade show" role="alert">
          {this.state.alertMessage}
          <button type="button" className="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      )
    } else {
      return ""
    }
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div>{this.handleAlertMessage()}</div>
        <div className="form-row">
          <div className="form-group col-md-6">
            <label>{this.props.labels["title"]}</label><span className="text-danger">* {this.state.emptyTitle ? this.props.labels["required_field"] : ''}</span>
            <input
              name="title"
              type="text"
              value={this.state.title}
              onChange={this.handleInputChange}
              onBlur={this.handleTitleBlur}
              className="form-control" />
          </div>
          <div className="form-group col-md-6">
            <label>{this.props.labels["recipient"]}</label>
            <input
              name="recipient"
              type="text"
              value={this.state.recipient}
              onChange={this.handleInputChange}
              className="form-control" />
          </div>
        </div>

        <div className="form-row">
          <div className="form-group col-md-12">
            <label>{this.props.labels["text"]}</label>
            <textarea
              name="text"
              value={this.state.text}
              onChange={this.handleInputChange}
              className="form-control" />
          </div>
        </div>

        <div className="form-row">
          <div className="form-group col-md-2">
            <label>{this.props.labels["year"]}</label>
            <SelectYear
              selected={this.state.year}
              onValueChange={this.handleInputChange} />
          </div>
          <div className="form-group col-md-2">
            <label>{this.props.labels["period"]}</label>
            <input
              name="period"
              type="text"
              value={this.state.period}
              onChange={this.handleInputChange}
              className="form-control" />
          </div>
          <div className="form-group col-md-2">
            <label>{this.props.labels["audit_type"]}</label>
            <DataSelect
              selected={String(this.state.audit_type_id)}
              onValueChange={this.handleInputChange}
              selectName="audit_type_id"
              dataUrl="audit_types.json" />
          </div>
          <div className="form-group col-md-6">
            <label>{this.props.labels["control_organization"]}</label>
            <DataSelect
              selected={String(this.state.control_organization_id)}
              onValueChange={this.handleInputChange}
              selectName="control_organization_id"
              dataUrl="organizations.json?type=control" />
          </div>
        </div>

        {this.organizationDisplay()}

        <div className="form-row">
          <div className="form-group col-md-3">
            <label>{this.props.labels["arrival_date"]}</label>
            <div className="input-group date datepicker" id="arrival_date" data-target-input="nearest" onBlur={this.handleArrivalDate}>
              <input value={this.state.arrival_date} name="arrival_date" id="arrival_date_input" type="text"  onChange={this.handleArrivalDate} className="form-control datetimepicker-input" data-target= "#arrival_date" />
              <div className="input-group-append" data-target="#arrival_date" data-toggle="datetimepicker">
                  <div className="input-group-text" ><i className="fa fa-calendar"></i></div>
              </div>
            </div>
          </div>
          <div className="form-group col-md-3">
            <label>{this.props.labels["assignment_date"]}</label>
            <div className="input-group date datepicker" id="assignment_date" data-target-input="nearest" onBlur={this.handleAssignmentDate}>
              <input value={this.state.assignment_date} name="assignment_date" id="assignment_date_input" type="text"  onChange={this.handleAssignmentDate} className="form-control datetimepicker-input" data-target= "#assignment_date" />
              <div className="input-group-append" data-target="#assignment_date" data-toggle="datetimepicker">
                  <div className="input-group-text" ><i className="fa fa-calendar"></i></div>
              </div>
            </div>
          </div>
          <div className="form-group col-md-3">
            <label>{this.props.labels["received_date"]}</label>
            <div className="input-group date datepicker" id="received_date" data-target-input="nearest" onBlur={this.handleReceivedDate}>
              <input value={this.state.received_date} name="received_date" id="received_date_input" type="text"  onChange={this.handleReceivedDate} className="form-control datetimepicker-input" data-target= "#received_date" />
              <div className="input-group-append" data-target="#received_date" data-toggle="datetimepicker">
                  <div className="input-group-text" ><i className="fa fa-calendar"></i></div>
              </div>
            </div>
          </div>
          <div className="form-group col-md-3">
            <label>{this.props.labels["confirmation_date"]}</label>
            <div className="input-group date datepicker" id="confirmation_date" data-target-input="nearest" onBlur={this.handleConfirmationDate}>
              <input value={this.state.confirmation_date} name="confirmation_date" id="confirmation_date_input" type="text"  onChange={this.handleConfirmationDate} className="form-control datetimepicker-input" data-target= "#confirmation_date" />
              <div className="input-group-append" data-target="#confirmation_date" data-toggle="datetimepicker">
                  <div className="input-group-text" ><i className="fa fa-calendar"></i></div>
              </div>
            </div>
          </div>
        </div>

        <div className="form-row">
          <div className="form-group col-md-6">
            <label>{this.props.labels["user"]}</label>
            <SelectUser
              selected={String(this.state.user_id)}
              onValueChange={this.handleInputChange} />
          </div>
          <div className="form-group col-md-6">
            <label>{this.props.labels["sensitivity"]}</label>
            <SelectSensitivity
              selected={this.state.sensitivity}
              onValueChange={this.handleInputChange} />
          </div>
        </div>

        <input type="submit" value="Valider" className="btn btn-primary" /> &nbsp;
        <a className="btn btn-secondary" href={"/audit_reports/" + this.state.id}>{this.props.labels["cancel"]}</a>
      </form>
    );
  }
}

AuditReportForm.propTypes = {
  auditReport: PropTypes.object,
  authenticityToken: PropTypes.string,
  labels: PropTypes.object,
};
export default AuditReportForm
