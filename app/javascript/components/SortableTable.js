import React from 'react';
import PropTypes from 'prop-types';
import DataTable from 'react-data-table-component';



class SortableTable extends React.Component {
  
  render () {
    const data = this.props.data;
    const columns = this.props.columns;
    const title = this.props.title;
    let headers = [];

    Object.keys(columns).map(function(key, index) {
      headers.push({name:columns[key], selector:key, sortable:true})
    });

    return (
      <DataTable
        title={title}
        striped={true}
        dense={true}
        noHeader={true}
        highlightOnHover={true}
        columns={headers}
        data={data}
        defaultSortField="name"
        pagination
      />
    );
  }
}

SortableTable.propTypes = {
  data: PropTypes.array,
  columns: PropTypes.object,
  title: PropTypes.string
};
export default SortableTable

