import React from "react"
import PropTypes from "prop-types"
import axios from 'axios'

class DataSelect extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      values: []
    }
  }

  handleChange(e) {
    this.props.onValueChange(e);
    defaultv=5
  }

  componentDidMount() {
    axios.defaults.headers.common['X-CSRF-TOKEN'] = this.props.authenticityToken;
    axios.get(`/${this.props.dataUrl}`)
    .then(res => {
      const values = res.data;
      this.setState({ values });
    })
    .catch(error => {
      console.log(error.response.data);
    }) 
  }

  render () {
    return (
      <select name={this.props.selectName} id={this.props.selectName} value={this.props.selected} onChange={this.handleChange} className="form-control">
        { this.state.values.map(v => v.name == "Contrôle Général des Services Publics" ? <option  value={v.id} key={v.id} selected>{v.name}</option>:'')}
        { this.state.values.map(v => v.name != "Contrôle Général des Services Publics" ? <option  value={v.id} key={v.id} selected>{v.name}</option>:'')}
      </select>
    )
    
  }
}

DataSelect.propTypes = {
  authenticityToken: PropTypes.string,
  selected: PropTypes.string,
  onValueChange: PropTypes.func,
  selectName: PropTypes.string,
  dataUrl: PropTypes.string,
};
export default DataSelect