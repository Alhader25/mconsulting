import React from "react"
import PropTypes from "prop-types"
import axios from 'axios'

class SelectUser extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      users: []
    }
  }

  handleChange(e) {
    this.props.onValueChange(e);
  }

  componentDidMount() {
    axios.defaults.headers.common['X-CSRF-TOKEN'] = this.props.authenticityToken;
    axios.get(`/users.json`)
    .then(res => {
      const users = res.data;
      this.setState({ users });
    })
    .catch(error => {
      console.log(error.response.data);
    }) 
  }

  render () {
    return (
      <select name="user_id" id="user_id" value={this.props.selected} onChange={this.handleChange} className="form-control">
        { this.state.users.map(u =>  <option value={u.id} key={u.id}>{u.first_name+" "+u.last_name}</option>)}
      </select>
    )
  }
}

SelectUser.propTypes = {
  authenticityToken: PropTypes.string,
  selected: PropTypes.string,
  onValueChange: PropTypes.func,
};
export default SelectUser