import React from "react"
import PropTypes from "prop-types"
import axios from 'axios'

class SelectSensitivity extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      sensitivities: {}
    }
  }

  handleChange(e) {
    this.props.onValueChange(e);
  }

  componentDidMount() {
    axios.defaults.headers.common['X-CSRF-TOKEN'] = this.props.authenticityToken;
    axios.get(`/sensitivities`)
    .then(res => {
      const sensitivities = res.data;
      this.setState({ sensitivities });
    })
    .catch(error => {
      console.log(error.response.data);
    }) 
  }

  render () {
    return (
      <select name="sensitivity" id="sensitivity" value={this.props.selected} onChange={this.handleChange} className="form-control">
        { Object.keys(this.state.sensitivities).map((k) =>  <option key={k} value={k}>{this.state.sensitivities[k]}</option>)}
      </select>
    )
  }
}

SelectSensitivity.propTypes = {
  authenticityToken: PropTypes.string,
  selected: PropTypes.string,
  onValueChange: PropTypes.func,
};
export default SelectSensitivity