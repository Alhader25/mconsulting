import React from "react"
import PropTypes from "prop-types"

class SelectYear extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.props.onValueChange(e);
  }


  render () {
    const currentYear = new Date().getFullYear();
    let yearsArray = [];
    for (let i = currentYear -10; i<=currentYear + 10; i++){
      yearsArray.push(i);
    }

    let years = yearsArray.map((y) => { 
      return (
        <option value={y} key={y}>{y}</option>
      ) 
    });
    return (
      <select name="year" value={this.props.selected} onChange={this.handleChange} className="form-control">
          {years}
      </select>
    )
  }
}

SelectYear.propTypes = {
  authenticityToken: PropTypes.string,
  selected: PropTypes.number,
  onValueChange: PropTypes.func,
};
export default SelectYear