class ApplicationController < ActionController::Base
    include Pundit
    before_action :set_locale
    before_action :authenticate_user!
    protect_from_forgery

    # https://guides.rubyonrails.org/i18n.html#managing-the-locale-across-requests
    # set locale using a url query param (e.g. http://example.com/books?locale=fr)
    # many more options for setting locale in the link above
    def set_locale
      I18n.locale = params[:locale] || I18n.default_locale
    end
end
