class EvidencesController < ApplicationController
  before_action :set_evidence, only: [:show, :edit, :update, :destroy]

  def show
    authorize @evidence
    @recommendation = Recommendation.new()
    @recommendation.audit_report = @audit_report
  end

  def new
    @audit_report = AuditReport.find(params[:audit_report_id])
    @evidence = Evidence.new
    @evidence.audit_report = @audit_report
    authorize @evidence
  end

  def edit
    authorize @evidence
  end

  def create
    @audit_report = AuditReport.find(params[:audit_report_id])
    @evidence = @audit_report.evidences.create(evidence_params)
    authorize @evidence

    if @evidence.save
      flash[:notice] = I18n.t("evidence.save_success")
      redirect_to audit_report_path(@audit_report)
    else
      render 'new'
    end
  end

  def update
      authorize @evidence
       
      if @evidence.update(evidence_params)
        flash[:notice] = I18n.t("evidence.save_success")
        redirect_to @audit_report
      else
        render 'edit'
      end
    end

  def destroy
    authorize @evidence
    @evidence.destroy
    redirect_to audit_report_path(@audit_report)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_evidence
      @evidence = Evidence.find(params[:id])
      @audit_report = @evidence.audit_report
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def evidence_params
      params.require(:evidence).permit(:title, :text, :audit_report_id)
    end
end