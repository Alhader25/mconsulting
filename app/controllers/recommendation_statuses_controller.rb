class RecommendationStatusesController < ApplicationController
  before_action :set_recommendation_status, only: [:show, :edit, :update, :destroy]

  # GET /recommendation_statuses
  def index
    @recommendation_statuses = RecommendationStatus.order(:name)
    authorize @recommendation_statuses
  end

  # GET /recommendation_statuses/new
  def new
    @recommendation_status = RecommendationStatus.new
    authorize @recommendation_status
  end

  # GET /recommendation_statuses/1/edit
  def edit
    authorize @recommendation_status
  end

  # POST /recommendation_statuses
  def create
    @recommendation_status = RecommendationStatus.new(recommendation_status_params)
    authorize @recommendation_status

    if @recommendation_status.save
      redirect_to recommendation_statuses_path
    else
      render 'new'
    end
  end

  # PATCH/PUT /recommendation_statuses/1
  def update
    authorize @recommendation_status
    if @recommendation_status.update(recommendation_status_params)
      redirect_to recommendation_statuses_path
    else
      render 'edit'
    end
  end

  # DELETE /recommendation_statuses/1
  def destroy
    authorize @recommendation_status
    if @recommendation_status.destroy
      redirect_to recommendation_statuses_path
    else
      render 'edit'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_recommendation_status
      @recommendation_status = RecommendationStatus.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def recommendation_status_params
      params.require(:recommendation_status).permit(:name)
    end
end
