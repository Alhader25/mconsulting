class AuditReportsController < ApplicationController
    def index

     if current_user.user?
            params[:user] = current_user.id.to_s
        end
        @users=User.all
        if params[:user]
          @audit_reports = AuditReport.joins(:control_organization, {verified_organization: :ministry}, :audit_type, :user).filters(params.slice(:user, :control, :verified, :year, :ministry)).includes(:control_organization, {verified_organization: :ministry}, :audit_type, :user)
        end
      
    end

    def sensitivities
        @sensitivities = Hash[AuditReport.sensitivities.map { |k,v| [v, AuditReport.human_attribute_name("sensitivity.#{k}")] }]
        render json: @sensitivities, status: :ok
    end

    def show
        @audit_report = AuditReport.find(params[:id])
        authorize @audit_report
        @recommendation = Recommendation.new()
        @recommendation.audit_report = @audit_report
        @evidence = Evidence.new()
        @evidence.audit_report = @audit_report

    end

    def new
        @audit_report = AuditReport.new
        authorize @audit_report
        @labels = AuditReport.get_form_labels
    end

    def edit
        @audit_report = AuditReport.find(params[:id])
        authorize @audit_report
        @labels = AuditReport.get_form_labels
    end

    def create
        @audit_report = AuditReport.new(audit_report_params)
        authorize @audit_report
        if @audit_report.save
          render json: @audit_report, status: :ok
        else
          render json: @audit_report.errors, status: :unprocessable_entity
        end
    end

    def update
        @audit_report = AuditReport.find(params[:id])
        authorize @audit_report
        unless @audit_report.update(audit_report_params)
          render json: @audit_report.errors, status: :unprocessable_entity
        end
    end

    def destroy
        @audit_report = AuditReport.find(params[:id])
        @audit_report.destroy
        redirect_to audit_reports_path
    end
    #alhader
  
  
  def import
    require 'csv'
    require 'roo'
    #@audit_report= AuditReport.import(params[:file])
    spreadsheet =Roo::Spreadsheet.open(params[:file])
    header = spreadsheet.row(1)
    i=0
   spreadsheet.each do |row|
    i+=1
        control=Organization.where({abbreviation: row[1]}).or(Organization.where({name: row[1]})).first
        entite=Organization.where({abbreviation: row[2]}).or(Organization.where({name: row[2]})).first
        audit=AuditType.where({name:row[3]}).first
        user=User.where({email:row[5]}).first

        @audit_report= AuditReport.new
        @audit_report.title=row[0]
        if defined?(control.id)
           @audit_report.control_organization_id=control.id
        end
        if defined?(entite.id)
           @audit_report.verified_organization_id=entite.id
        end
        if defined?(audit.id)
           @audit_report.audit_type_id=audit.id
        end
        if defined?(user.id)
           @audit_report.user_id=user.id
        end
        @audit_report.year=row[4]
        @audit_report.save      
        if @audit_report.errors.any?
          @audit_report.errors.add :base, :invalid, message: "Ligne en question "+i.to_s
        else
          redirect_to root_url, notice: "Rapports importés avec succès."
        end
    end  
      
    end
  
      private
          def audit_report_params
              params.require(:audit_report).permit(:title, :text, :year, :period, :control_organization_id, 
                  :verified_organization_id, :audit_type_id, :arrival_date, :assignment_date, :received_date, 
                  :confirmation_date, :completion_date, :user_id, :sensitivity, :recipient, {reports: []})
          end
  end