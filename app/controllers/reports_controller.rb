class ReportsController < ApplicationController

  # GET /reports/ministries/control
  def control_ministries
    @ministries = Ministry.control_with_aggregates(params[:year], params[:status])
  end

  # GET /reports/ministries/verified
  def verified_ministries
    @ministries = Ministry.verified_with_aggregates(params[:year], params[:status])
  end

  # GET /reports/organizations/control
  def control_organizations
    @organizations = Organization.control_with_aggregates(params[:year], params[:status])
  end

  # GET /reports/organizations/verified
  def verified_organizations
    @organizations = Organization.verified_with_aggregates(params[:year], params[:status], params[:ministry])
  end

  # GET /reports/categories/rootstatus
  def root_category_totals
    @categories = Category.aggregates_by_root_status(params[:year])
  end

  # GET /reports/categories/rootstatus/csv
  def root_category_totals_csv
    @categories = Category.aggregates_by_root_status(params[:year], to_csv=true)
    send_data @categories, :filename => 'root_category_totals.csv'
  end

  # GET /reports/categories/rootstatus/summary
  def root_category_totals_summary
    @categories = Category.aggregates_by_root_status(params[:year], to_csv=false, to_summary_csv=true)
    @counts = AuditReport.summary_counts(params[:year])

    puts @counts

    respond_to do |format|
      format.xlsx {
        response.headers[
          'Content-Disposition'
        ] = "attachment; filename='root_category_summary.xlsx'"
      }
    end
  end

  # GET /reports/recommendations/category
  def recommendation_categories
    @recommendations = Recommendation.recommendations_with_categories(params[:year], params[:status])
  end

  # GET /reports/recommendations/category/csv
  def recommendation_categories_csv
    @recommendations = Recommendation.recommendations_with_categories(params[:year], params[:status], to_csv=true)
    send_data @recommendations, :filename => 'recommendation_categories.csv'
  end

  # GET /reports/recommendations/category/summary
  def recommendation_categories_summary
    @recommendations = Recommendation.recommendations_with_categories(params[:year], params[:status], to_csv=false, to_summary=true)
    respond_to do |format|
      format.xlsx {
        response.headers[
          'Content-Disposition'
        ] = "attachment; filename='categories_summary.xlsx'"
      }
    end
  end

  # GET /reports/exports
  def exports
    
  end

  # GET /reports/exports/all/csv
  def export_all_csv
    @audit_reports = AuditReport.export_all
    send_data @audit_reports, :filename => 'export_all.csv'
  end
  #Alhader
  def rapport
    @categories = Category.aggregates_by_root_status(params[:year], to_csv=false, to_summary_csv=true)
    @counts = AuditReport.summary_counts(params[:year])
    @recommendations = Recommendation.recommendations_with_categories(params[:year], params[:status], to_csv=false, to_summary=true)
  end
  
end
