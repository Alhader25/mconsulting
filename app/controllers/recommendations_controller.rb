class RecommendationsController < ApplicationController
    def index
      if current_user.user?
        params[:user] = current_user.id.to_s
      end
      @users=User.all
      @reports=AuditReport.all
      if params[:user]
        @recommendations = Recommendation.filtered_recommendations(params.slice(:user, :control, :verified, :year, :status, :sensitivity,:ministry,:report))
      end
      respond_to do |format|
        format.xlsx {
          response.headers[
            'Content-Disposition'
          ] = "attachment; filename=recommendation.xlsx"
        }
        format.html { render :index }
      end
    end

    def export_csv
      authorize  Recommendation.new
      @recommendations = Recommendation.filtered_recommendations(params.slice(:user, :control, :verified, :year, :status, :sensitivity,:ministry,:report), to_csv=true)
      send_data @recommendations, :filename => 'recommendations.csv'
    end

    def annexe 
      @categories = Category.aggregates_by_root_status(params[:year], to_csv=false, to_summary_csv=true)
      @counts = AuditReport.summary_counts(params[:year])
      @reports=AuditReport.all
      @recommendations = Recommendation.filtered_recommendations(params.slice(:user, :control, :verified, :year, :status, :sensitivity,:ministry,:report))
      render 'annexe'
    end

    def show
      @audit_report = AuditReport.find(params[:audit_report_id])
      @recommendation = Recommendation.find(params[:id])
      authorize @recommendation
    end

    def new
      @audit_report = AuditReport.find(params[:audit_report_id])
      @recommendation = Recommendation.new
      @recommendation.audit_report = @audit_report
      authorize @recommendation
    end

    def edit
      @audit_report = AuditReport.find(params[:audit_report_id])
      @recommendation = Recommendation.find(params[:id])
      authorize @recommendation
    end

    def create
      @audit_report = AuditReport.find(params[:audit_report_id])
      @recommendation = @audit_report.recommendations.create(recommendation_params)
      authorize @recommendation

      if @recommendation.save
        flash[:notice] = I18n.t("recommendation.save_success")
        redirect_to action: "edit", id: @recommendation.id, audit_report_id: params[:audit_report_id]
        #redirect_to audit_report_path(@audit_report)
      else
        render 'new'
      end
    end

    def update
      @audit_report = AuditReport.find(params[:audit_report_id])
      @recommendation = Recommendation.find(params[:id])
      authorize @recommendation
       
      if @recommendation.update(recommendation_params)
        flash[:notice] = I18n.t("recommendation.save_success")
        redirect_to action: "edit", id: params[:id], audit_report_id: params[:audit_report_id]
        #redirect_to @audit_report
      else
        render 'edit'
      end
    end

    def destroy
      @audit_report = AuditReport.find(params[:audit_report_id])
      @recommendation = @audit_report.recommendations.find(params[:id])
      authorize @recommendation
      @recommendation.destroy
      redirect_to audit_report_path(@audit_report)
    end
    #alhader
    def rapport
      @categories = Category.aggregates_by_root_status(params[:year], to_csv=false, to_summary_csv=true)
      @counts = AuditReport.summary_counts(params[:year])
      @recommendations = Recommendation.recommendations_with_categories(params[:year], params[:status], to_csv=false, to_summary=true)
    end
     
     private
        def recommendation_params
          params.require(:recommendation).permit(:title, :evidence_id, :text, :year, :reason, :justification, 
            :response, :cost, :recovered, :status, :recommendation_status_id, :category_ids => [])
        end
end
