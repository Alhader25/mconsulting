class CategoriesController < ApplicationController
  def index
    @categories = Category.order(:name)
    authorize @categories
    @categories = @categories.arrange_as_array
  end

  def new
    @category = Category.new
    authorize @category
  end

  def create
    @category = Category.new(category_params)
    authorize @category
    if @category.save
      redirect_to categories_path
    else
      render 'new'
    end
  end

  def edit
    @category = Category.find(params[:id])
    authorize @category
  end

  def update
    @category = Category.find(params[:id])
    authorize @category
    
    if @category.update(category_params)
      redirect_to categories_path
    else
      render 'edit'
    end
  end

  def destroy
    @category = Category.find(params[:id])
    authorize @category
    if @category.destroy
      redirect_to categories_path
    else
      render 'edit'
    end
  end 
  def import
    xlsx = Roo::Spreadsheet.open(params[:file])
    xlsx.each do |row|
        c=Category.new
        c.name=row[0]
        search=Category.where(name:row[1]).first
        if defined?(search.id)
        c.ancestry=search.id
        end
        c.save
             
         
    end
  end
  private

  def category_params
     params.require(:category).permit(:name, :parent_id)
  end
end
