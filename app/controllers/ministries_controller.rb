class MinistriesController < ApplicationController
  before_action :set_ministry, only: [:show, :edit, :update, :destroy]

  # GET /ministries
  # GET /ministries.json
  def index
    @ministries = Ministry.order(:name)
    authorize @ministries
    respond_to do |format|
      format.html
      format.json { render json: @ministries, status: :ok }
    end
  end

  # GET /ministries/new
  def new
    @ministry = Ministry.new
    authorize @ministry
  end

  # GET /ministries/1/edit
  def edit
    authorize @ministry
  end

  # POST /ministries
  # POST /ministries.json
  def create
    @ministry = Ministry.new(ministry_params)
    authorize @ministry

    if @ministry.save
      redirect_to ministries_path
    else
      render 'new'
    end
  end

  # PATCH/PUT /ministries/1
  # PATCH/PUT /ministries/1.json
  def update
    authorize @ministry
    if @ministry.update(ministry_params)
      redirect_to ministries_path
    else
      render 'edit'
    end
  end

  # DELETE /ministries/1
  # DELETE /ministries/1.json
  def destroy
    authorize @ministry
    if @ministry.destroy
      redirect_to ministries_path
    else
      render 'edit'
    end
  end
#alhader

def import
  require 'roo'
  #Ministry.import(params[:file])
  xlsx = Roo::Spreadsheet.open(params[:file])
  xlsx.each do |row|
      m=Ministry.new
      m.abbreviation=row[0]
      m.name=row[1]
      m.save
  end
  redirect_to root_url, notice: "Ministères importés"
end



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ministry
      @ministry = Ministry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ministry_params
      params.require(:ministry).permit(:name, :abbreviation)
    end
end
