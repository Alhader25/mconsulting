class AuditTypesController < ApplicationController
  def index
    @audit_types = AuditType.order(:name)
    authorize @audit_types
    respond_to do |format|
      format.html
      format.json { render json: @audit_types, status: :ok }
    end
  end

  def new
    @audit_type = AuditType.new
    authorize @audit_type
  end

  def create
    @audit_type = AuditType.new(audit_type_params)
    authorize @audit_type
    if @audit_type.save
      redirect_to audit_types_path
    else
      render 'new'
    end
  end

  def edit
    @audit_type = AuditType.find(params[:id])
    authorize @audit_type
  end

  def update
    @audit_type = AuditType.find(params[:id])
    authorize @audit_type
    
    if @audit_type.update(audit_type_params)
      redirect_to audit_types_path
    else
      render 'edit'
    end
  end

  def destroy
    @audit_type = AuditType.find(params[:id])
    authorize @audit_type
    if @audit_type.destroy
      redirect_to audit_types_path
    else
      render 'edit'
    end
  end 
  private

  def audit_type_params
     params.require(:audit_type).permit(:name)
  end
end
