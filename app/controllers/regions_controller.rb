class RegionsController < ApplicationController
  before_action :set_region, only: [:show, :edit, :update, :destroy]

  # GET /regions
  def index
    @regions = Region.order(:name)
    authorize @regions
    respond_to do |format|
      format.html
      format.json { render json: @regions, status: :ok }
    end
  end

  # GET /regions/new
  def new
    @region = Region.new
    authorize @region
  end

  # GET /regions/1/edit
  def edit
    authorize @region
  end

  # POST /regions
  def create
    @region = Region.new(region_params)
    authorize @region
    if @region.save
      redirect_to regions_path
    else
      render 'new'
    end
  end

  # PATCH/PUT /regions/1
  def update
    authorize @region
    if @region.update(region_params)
      redirect_to regions_path
    else
      render 'edit'
    end
  end

  # DELETE /regions/1
  def destroy
    authorize @region
    if @region.destroy
      redirect_to regions_path
    else
      render 'edit'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_region
      @region = Region.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def region_params
      params.require(:region).permit(:name)
    end
end
