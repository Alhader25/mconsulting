class OrganizationsController < ApplicationController
  def index
    if params[:type] == 'verified'
      @organizations = Organization.verified().order(:name)
    elsif params[:type] == 'control'
      @organizations = Organization.control().order(:name)
    else
      @organizations = Organization.order(:name)
    end
    authorize @organizations
    respond_to do |format|
      format.html
      format.json { render json: @organizations, status: :ok }
    end
  end

  def new
    @organization = Organization.new
    authorize @organization
    @org_names = Organization.pluck(:name).to_json
  end

  def create
    @organization = Organization.new(organization_params)
    authorize @organization
    if @organization.save
      respond_to do |format|
        format.html { redirect_to organizations_path }
        format.json { render json: @organization, status: :ok }
      end
    else
      respond_to do |format|
        format.html { render 'new' }
        format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @organization = Organization.find(params[:id])
    authorize @organization
    @org_names = Organization.pluck(:name).to_json
  end

  def update
    @organization = Organization.find(params[:id])
    authorize @organization
    
    if @organization.update(organization_params)
      redirect_to organizations_path
    else
      render 'edit'
    end
  end

  def import
    Organization.import(params[:file])
    redirect_to root_url, notice: "Entitées importés"
  end

  def destroy
    @organization = Organization.find(params[:id])
    authorize @organization
    if @organization.destroy
      redirect_to organizations_path
    else
      @org_names = Organization.pluck(:name).to_json
      render 'edit'
    end
  end 
  private

  def organization_params
     params.require(:organization).permit(:name, :abbreviation, :control, :ministry_id, :region_id)
  end
end
