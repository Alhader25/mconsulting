class UsersController < ApplicationController
  def show
    @user = User.find(params[:id])
    authorize @user
  end

  def index
    @users = User.where.not(:id => current_user.id)
    authorize @users
    respond_to do |format|
      format.html
      format.json { render json: @users, status: :ok }
    end
  end

  def new
    @user = User.new
    authorize @user
  end

  def create
    @user = User.new(user_params)
    @user.role = 'admin'
    authorize @user
    if @user.save
      redirect_to @user
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
    authorize @user
  end

  def update
    @user = User.find(params[:id])
    authorize @user
    params[:user].delete(:role) if @user == current_user 
    params[:user].delete(:password) if params[:user][:password].blank? 
    params[:user].delete(:password_confirmation) if params[:user][:password].blank? and params[:user][:password_confirmation].blank?
    
    if @user.update(user_params)
      redirect_to @user
    else
      render 'edit'
    end
  end

  def destroy
    @user = User.find(params[:id])
    authorize @user
    if @user.destroy
      redirect_to users_path
    else
      render 'edit'
    end
  end 
  private

  def user_params
     params.require(:user).permit(:email, :password, :password_confirmation, :first_name, :last_name, :organization_id, :role)
  end
end