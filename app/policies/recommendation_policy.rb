class RecommendationPolicy < ApplicationPolicy
    attr_reader :user, :recommendation

    def create?
      user.admin? || record.audit_report.user == user
    end
  
    def update?
      user.admin? || record.audit_report.user == user
    end

    def show?
      user.admin? || user.restricted_user? || record.audit_report.user == user
    end

    def destroy?
      user.admin? || record.audit_report.user == user
    end

    def export_csv?
      user.admin?
    end

end