class UserPolicy < ApplicationPolicy
  attr_reader :user, :user

  def update?
    user.admin? || record == user
  end

  def show?
    user.admin? || record == user
  end

end