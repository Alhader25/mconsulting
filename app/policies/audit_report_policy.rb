class AuditReportPolicy < ApplicationPolicy
    attr_reader :user, :audit_report

    def show?
      user.admin? || user.restricted_user? || record.user == user
    end


end