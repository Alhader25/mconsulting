class EvidencePolicy < ApplicationPolicy
    attr_reader :user, :evidence

    def create?
      user.admin? || record.audit_report.user == user
    end
  
    def update?
      user.admin? || record.audit_report.user == user
    end

    def show?
      user.admin? || user.restricted_user? || record.audit_report.user == user
    end

    def destroy?
      user.admin? || record.audit_report.user == user
    end

end