class Category < ApplicationRecord
  has_ancestry

  has_and_belongs_to_many :recommendations
  validates_uniqueness_of :name,:message=>"Erreur de doublon"
  validates :name, presence: true
  def self.arrange_as_array(options={}, hash=nil)                                                                                                                                                            
    hash ||= arrange(options)

    arr = []
    hash.each do |node, children|
      arr << node
      arr += arrange_as_array(options, children) unless children.nil? || children.empty?
    end
    arr
  end

  def name_for_selects
    "#{'--' * depth} #{name}"
  end

  def possible_parents
    parents = Category.arrange_as_array(:order => 'name')
    return new_record? ? parents : parents - subtree
  end

  #===== REPORT FUNCTIONS ======
  def self.aggregates_by_root_status(year, to_csv=false, to_summary_csv=false)
    columns = {
      "name":I18n.t("activerecord.models.category"),
      "status":I18n.t('activerecord.attributes.recommendation.status'),
      "status_count":I18n.t('reports.count'),
      "cost_by_status":I18n.t('reports.total_cost'),
      "recovered_by_status":I18n.t('reports.total_recovered'),
      "category_total":I18n.t('reports.category_count')
    }

    categories = "
      WITH statuses AS (
        SELECT 
          id,
          name
        FROM recommendation_statuses
      ),
      status_categories AS (
        SELECT 
          c.id AS root_category_id, 
          c.name AS root_category_name, 
          s.name AS status,
          s.id as status_id
        FROM categories c
        CROSS JOIN statuses s
        WHERE c.ancestry is null
      ),
      root_categories AS (
        SELECT  
          c2.id AS category_id, 
          c1.id AS root_category_id, 
          c2.name AS category_name, 
          c1.name AS root_category_name
        FROM public.categories c1 
        INNER JOIN public.categories c2 ON c2.ancestry = c1.id::text OR c2.ancestry LIKE c1.id::text || '/%' 
        UNION
        SELECT 
          id AS category_id, 
          id AS root_category_id, 
          name AS category_name, 
          name AS root_category_name
        FROM public.categories 
        WHERE ancestry is null
      ),
      root_counts AS (
        SELECT 
          rc.root_category_id, 
          COUNT(DISTINCT cr.recommendation_id) AS count_by_root
        FROM root_categories rc
        INNER JOIN categories_recommendations cr ON rc.category_id = cr.category_id
        INNER JOIN recommendations r ON r.id = cr.recommendation_id
        INNER JOIN audit_reports ar ON r.audit_report_id = ar.id
        WHERE 0=0
        "
        categories = categories + " AND ar.year= " + year if !year.nil? && year.length > 0
        categories = categories + "
        GROUP BY rc.root_category_id
      ),
      recommendations AS (
        SELECT DISTINCT 
          r.id, 
          ar.year, 
          r.title, 
          r.cost, 
          r.recovered, 
          rs.name as status, 
          rs.id as status_id,
          rc.root_category_name, 
          rc.root_category_id, 
          r.audit_report_id, 
          r.evidence_id
        FROM recommendations r 
        INNER JOIN audit_reports ar ON r.audit_report_id = ar.id
        INNER JOIN categories_recommendations cr ON cr.recommendation_id = r.id
        INNER JOIN recommendation_statuses rs ON rs.id = r.recommendation_status_id
        INNER JOIN root_categories rc ON rc.category_id = cr.category_id
        WHERE 0=0
        "
        categories = categories + " AND ar.year= " + year if !year.nil? && year.length > 0
        categories = categories + "
        ORDER BY audit_report_id, id, root_category_id
      )
      SELECT 
        sc.root_category_id AS id, 
        sc.root_category_name AS name,  
        sc.status, 
        COUNT(r.status) AS status_count, 
        COALESCE(SUM(r.cost), 0)::numeric::bigint   AS cost_by_status, 
        COALESCE(SUM(r.recovered), 0)::numeric::bigint AS recovered_by_status, 
        COALESCE(rc.count_by_root, 0) AS category_total
      FROM status_categories sc
      LEFT JOIN root_counts rc ON sc.root_category_id = rc.root_category_id
      LEFT JOIN recommendations r ON sc.root_category_id = r.root_category_id and sc.status_id = r.status_id
      GROUP BY sc.root_category_id, sc.root_category_name, sc.status, rc.count_by_root
      ORDER BY sc.root_category_id, sc.status;
      "

    data = Category.find_by_sql(categories)
  
   data.each.with_index(1) do |cat, i|
      cat.id = i
    end

    if to_csv
      CSV.generate do |csv|
        csv << [I18n.t("activerecord.models.category"), I18n.t('activerecord.attributes.recommendation.status'), I18n.t('reports.count'), I18n.t('reports.total_cost'), I18n.t('reports.total_recovered'), I18n.t('reports.category_count')]
        data.each do |c|
          csv << [c.name, c.status, c.status_count, c.cost_by_status, c.recovered_by_status, c.category_total]
        end
      end
    elsif to_summary_csv
      cats = {}
      data.each do |c|
        cats[c.name] = {} if !cats[c.name]
        cats[c.name][c.status] = {} if !cats[c.name][c.status]
        cats[c.name][c.status]["count"] = c.status_count
        cats[c.name]["count"] = c.category_total
      end

      return cats

    else 
      return {:data => data, :columns=> columns}
    end 
  end
  
end
