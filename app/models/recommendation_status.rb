class RecommendationStatus < ApplicationRecord
  has_many :recommendations, dependent: :restrict_with_error

  validates :name, presence: true
  validates_uniqueness_of :name,:message=>"Erreur de doublon"
  def self.to_option_values
    rs_objects = self.order(:name)
    statuses = Hash.new
    
    rs_objects.each do |s|
      statuses[s.name] = s.id
    end
    return statuses
  end
end
