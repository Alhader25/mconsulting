class Region < ApplicationRecord
  has_many :organizations, dependent: :restrict_with_error

  validates :name, presence: true
  validates_uniqueness_of :name,:message=>"Erreur de doublon " 
end
