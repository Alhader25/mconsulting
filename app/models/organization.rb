class Organization < ApplicationRecord
  scope :control, -> () { where control: true }
  scope :verified, -> () { where control: false }
  has_many :control_audit_reports, :class_name => 'AuditReport', :foreign_key => 'control_organization_id', dependent: :restrict_with_error
  has_many :verified_audit_reports, :class_name => 'AuditReport', :foreign_key => 'verified_organization_id', dependent: :restrict_with_error
  has_many :users, dependent: :restrict_with_error
  belongs_to :ministry
  belongs_to :region

  validates :name, presence: true
  validates_uniqueness_of :name,:message=>"Erreur de doublon "
  def name_with_abbreviation
    "#{abbreviation}: #{name}"
  end

  def self.to_option_values
    org_objects = self.order(:name)
    orgs = Hash.new
    
    org_objects.each do |o|
      orgs[o.name_with_abbreviation] = o.id
    end
    return orgs
  end

    #===== REPORT FUNCTIONS ======
  def self.verified_with_aggregates(year, status, ministry)
    columns = {
      "name":I18n.t('activerecord.attributes.ministry.name'),
      "audit_reports_count":I18n.t('reports.audit_reports_count'),
      "recommendations_count":I18n.t('reports.recommendations_count'),
      "total_cost":I18n.t('reports.total_cost'),
      "total_recovered":I18n.t('reports.total_recovered')
    }

    organizations = "
      SELECT 
        o.id,
        o.name,
        o.abbreviation,
        o.control,
        o.ministry_id,
        COUNT(DISTINCT ar.id) as audit_reports_count,
        COUNT(r.id) as recommendations_count,
        SUM(r.cost)::numeric::bigint as total_cost,
        SUM(r.recovered)::numeric::bigint as total_recovered
      FROM
        organizations o
      INNER JOIN audit_reports ar ON ar.verified_organization_id = o.id
      INNER JOIN recommendations r ON r.audit_report_id = ar.id
      INNER JOIN recommendation_statuses rs ON rs.id = r.recommendation_status_id
      WHERE 0=0
      "

    organizations = organizations + " AND ar.year= " + year if !year.nil? && year.length > 0
    organizations = organizations + " AND rs.id= " + status  if !status.nil? && status.length > 0
    organizations = organizations + " AND o.ministry_id = '" + ministry + "'" if !ministry.nil? && ministry.length > 0

    organizations = organizations + "
      GROUP BY o.id, o.name, o.abbreviation, o.ministry_id
      ORDER BY o.name asc
      "

    data = Organization.find_by_sql(organizations)

    return {:data => data, :columns=> columns}
  end

  def self.control_with_aggregates(year, status)
    columns = {
      "name":I18n.t('activerecord.attributes.ministry.name'),
      "audit_reports_count":I18n.t('reports.audit_reports_count'),
      "recommendations_count":I18n.t('reports.recommendations_count'),
      "total_cost":I18n.t('reports.total_cost'),
      "total_recovered":I18n.t('reports.total_recovered')
    }

    organizations = "
      SELECT 
        o.id,
        o.name,
        o.abbreviation,
        o.control,
        o.ministry_id,
        COUNT(DISTINCT ar.id) as audit_reports_count,
        COUNT(r.id) as recommendations_count,
        SUM(r.cost)::numeric::bigint as total_cost,
        SUM(r.recovered)::numeric::bigint as total_recovered
      FROM
        organizations o
      INNER JOIN audit_reports ar ON ar.control_organization_id = o.id
      INNER JOIN recommendations r ON r.audit_report_id = ar.id
      INNER JOIN recommendation_statuses rs ON rs.id = r.recommendation_status_id
      WHERE 0=0
      "

    organizations = organizations + " AND ar.year= " + year if !year.nil? && year.length > 0
    organizations = organizations + " AND rs.id= " + status  if !status.nil? && status.length > 0

    organizations = organizations + "
      GROUP BY o.id, o.name, o.abbreviation, o.ministry_id
      ORDER BY o.name asc
      "
    
    data = Organization.find_by_sql(organizations)

    return {:data => data, :columns=> columns}
  end

  def self.import(file)
    xlsx = Roo::Spreadsheet.open(file.path)
    xlsx.each do |row|
        m=Ministry.where(abbreviation: row[0]).first
        o=Organization.new
        o.abbreviation=row[1]
        o.name=row[2]
        o.region_id=row[3]
        o.control=row[4]
        o.ministry_id=m.id
        o.save
    end
  end

end
