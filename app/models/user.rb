class User < ApplicationRecord
  #using postgreSQL enum type: https://www.sitepoint.com/enumerated-types-with-activerecord-and-postgresql/
  enum role: {
    admin:  'admin',
    user:   'user',
    restricted_user: 'restricted_user'
  }

  has_many :audit_reports, dependent: :restrict_with_error
  belongs_to :organization

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :email, presence: true
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :rememberable, :trackable, :validatable, :timeoutable

  app_role = {
      user:  'user',
      admin:   'admin',
      restricted_user: 'restricted_user'
  }

  def check_roles(current_user)
    unless current_user.admin?
      self.role = :user
    end
  end

  def self.to_option_values
    user_objects = self.order(:email)
    users = Hash.new
    user_objects.each do |u|
      users[u.first_name] = u.id
    end
    return users
  end

end
