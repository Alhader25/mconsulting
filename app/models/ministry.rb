class Ministry < ApplicationRecord
  has_many :organizations, dependent: :restrict_with_error

  validates :name, presence: true
  validates_uniqueness_of :name,:message=>"Erreur de doublon"
  def name_with_abbreviation
    "#{abbreviation}: #{name}"
  end

  def self.to_option_values
    ministry_objects = self.order(:name)
    ministries = Hash.new
    
    ministry_objects.each do |m|
      ministries[m.name_with_abbreviation] = m.id
    end
    return ministries
  end

  #===== REPORT FUNCTIONS ======
  def self.verified_with_aggregates(year, status)
    columns = {
      "name":I18n.t('activerecord.attributes.ministry.name'),
      "audit_reports_count":I18n.t('reports.audit_reports_count'),
      "recommendations_count":I18n.t('reports.recommendations_count'),
      "total_cost":I18n.t('reports.total_cost'),
      "total_recovered":I18n.t('reports.total_recovered')
    }

    ministries = "
      SELECT 
        m.id,
        m.name,
        m.abbreviation,
        COUNT(DISTINCT ar.id) as audit_reports_count,
        COUNT(r.id) as recommendations_count,
        SUM(r.cost)::numeric::bigint as total_cost,
        SUM(r.recovered)::numeric::bigint as total_recovered
      FROM
        ministries m
      INNER JOIN organizations o ON m.id = o.ministry_id
      INNER JOIN audit_reports ar ON ar.verified_organization_id = o.id
      INNER JOIN recommendations r ON r.audit_report_id = ar.id
      INNER JOIN recommendation_statuses rs ON rs.id = r.recommendation_status_id
      WHERE 0=0
      "
    
    ministries = ministries + " AND ar.year= " + year if !year.nil? && year.length > 0
    ministries = ministries + " AND rs.id= " + status  if !status.nil? && status.length > 0

    ministries = ministries + "
      GROUP BY m.id, m.name, m.abbreviation
      ORDER BY m.name asc
      "

    data = Ministry.find_by_sql(ministries)

    return {:data => data, :columns=> columns}
  end

  def self.control_with_aggregates(year, status)
    ministries = "
      SELECT 
        m.id,
        m.name,
        m.abbreviation,
        COUNT(DISTINCT ar.id) as audit_reports_count,
        COUNT(r.id) as recommendations_count,
        SUM(r.cost) as total_cost,
        SUM(r.recovered) as total_recovered
      FROM
        ministries m
      INNER JOIN organizations o ON m.id = o.ministry_id
      INNER JOIN audit_reports ar ON ar.control_organization_id = o.id
      INNER JOIN recommendations r ON r.audit_report_id = ar.id
      INNER JOIN recommendation_statuses rs ON rs.id = r.recommendation_status_id
      WHERE 0=0
      "
    
    ministries = ministries + " AND ar.year= " + year if !year.nil? && year.length > 0
    ministries = ministries + " AND rs.id= " + status  if !status.nil? && status.length > 0

    ministries = ministries + "
      GROUP BY m.id, m.name, m.abbreviation
      ORDER BY m.name asc
      "
    
    Ministry.find_by_sql(ministries)
  end
  def self.import(file)
    xlsx = Roo::Spreadsheet.open(file.path)
    xlsx.each do |row|
        m=Ministry.new
        m.abbreviation=row[0]
        m.name=row[1]
        m.save!
    end
  end
end
