class AuditReport < ApplicationRecord
  mount_uploaders :reports, ReportUploader

  scope :year, -> (year) { where year: year }
  scope :user, -> (user_id) { where user_id: user_id }
  scope :type, -> (audit_type_id) { where audit_type_id: audit_type_id }
  scope :sensitivity, -> (sensitivity) { where sensitivity: sensitivity }
  scope :control, -> (control_organization_id) { where control_organization_id: control_organization_id }
  scope :verified, -> (verified_organization_id) { where verified_organization_id: verified_organization_id }
  scope :ministry, -> (ministry_id) { joins('INNER JOIN organizations ON audit_reports.verified_organization_id = organizations.id').where('organizations.ministry_id = ' + ministry_id) }
  
  include Filterable
  
  #using postgreSQL enum type: https://www.sitepoint.com/enumerated-types-with-activerecord-and-postgresql/
  enum sensitivity: {
      high:  'high',
      low:   'low'
  }

  has_many :recommendations, dependent: :destroy
  has_many :evidences, dependent: :destroy
  

  belongs_to :control_organization, :class_name => 'Organization'
  belongs_to :verified_organization, :class_name => 'Organization'
  belongs_to :audit_type
  belongs_to :user

  validates :title, presence: true
  validates :control_organization, presence: true
  validates :year, presence: true
  validates_uniqueness_of :title,scope: [:year, :verified_organization_id,:control_organization_id,:audit_type_id]
  def self.get_form_labels
    labels = {
          :title => I18n.t('activerecord.attributes.audit_report.title'),
          :recipient => I18n.t('activerecord.attributes.audit_report.recipient'),
          :text => I18n.t('activerecord.attributes.audit_report.text'),
          :year => I18n.t('activerecord.attributes.audit_report.year'),
          :period => I18n.t('activerecord.attributes.audit_report.period'),
          :audit_type => I18n.t('activerecord.attributes.audit_report.audit_type'),
          :control_organization => I18n.t('activerecord.attributes.audit_report.control_organization'),
          :verified_organization => I18n.t('activerecord.attributes.audit_report.verified_organization'),
          :user => I18n.t('activerecord.attributes.audit_report.user'),
          :sensitivity => I18n.t('activerecord.attributes.audit_report.sensitivity'),
          :arrival_date => I18n.t('activerecord.attributes.audit_report.arrival_date'),
          :assignment_date => I18n.t('activerecord.attributes.audit_report.assignment_date'),
          :received_date => I18n.t('activerecord.attributes.audit_report.received_date'),
          :confirmation_date => I18n.t('activerecord.attributes.audit_report.confirmation_date'),
          :required_field => I18n.t('errors.required_field'),
          :required_fields => I18n.t('errors.required_fields'),
          :save_success => I18n.t("audit_report.save_success"),
          :save_fail => I18n.t("audit_report.save_fail"),
          :new => I18n.t("audit_report.new"),
          :edit => I18n.t("audit_report.edit"),
          :cancel => I18n.t("navigation.cancel"),
          :add_organization => I18n.t("organization.add"),
          :organization_name => I18n.t("activerecord.attributes.organization.name"),
          :organization_abbreviation => I18n.t("activerecord.attributes.organization.abbreviation"),
          :ministry => I18n.t("activerecord.models.ministry"),
          :region => I18n.t("activerecord.models.region"),
          :add => I18n.t("navigation.add"),
        }
  end

  #===== REPORT FUNCTIONS ======
  def self.summary_counts(year)
    counts = "
    WITH rec_counts AS (
      SELECT 
        COUNT(distinct ar.id) AS audit_report_count,
        COUNT(r.id) AS recommendation_count,
        COUNT (distinct o.id) AS entity_count,
        SUM(CASE WHEN rs.name = 'Aucune Réponse' THEN 1 ELSE 0 END) AS unknown_status_count,
        SUM(CASE WHEN rs.name = 'Exécutée' THEN 1 ELSE 0 END) AS executed_status_count,
        SUM(CASE WHEN rs.name = 'En Cours' THEN 1 ELSE 0 END) AS in_progress_status_count,
        SUM(CASE WHEN rs.name = 'Non Exécutée' THEN 1 ELSE 0 END) AS not_executed_status_count
      FROM audit_reports ar
      INNER JOIN recommendations r ON r.audit_report_id = ar.id
      INNER JOIN organizations o ON o.id = ar.verified_organization_id
      INNER JOIN audit_types at ON at.id = ar.audit_type_id 
      INNER JOIN recommendation_statuses rs ON rs.id = r.recommendation_status_id
      WHERE 0=0
      "
      counts = counts + " AND ar.year= " + year if !year.nil? && year.length > 0
      counts = counts + "
    ),
    ar_counts AS (
      SELECT
        SUM(CASE WHEN at.name = 'Performance' THEN 1 ELSE 0 END) AS performance_count,
        SUM(CASE WHEN at.name = 'Financier' THEN 1 ELSE 0 END) AS finance_count
        FROM audit_reports ar
        INNER JOIN audit_types at ON at.id = ar.audit_type_id 
      WHERE 0=0
      "
      counts = counts + " AND ar.year= " + year if !year.nil? && year.length > 0
      counts = counts + "
    )
    SELECT 
      audit_report_count,
      recommendation_count,
      entity_count,
      performance_count,
      finance_count,
      unknown_status_count,
      executed_status_count,
      in_progress_status_count,
      not_executed_status_count
    FROM rec_counts, ar_counts
    "

    data_array = ActiveRecord::Base.connection.execute(counts).values[0]

    data = {
        :audit_report_count => data_array[0], 
        :recommendation_count => data_array[1], 
        :entity_count => data_array[2], 
        :performance_count => data_array[3],
        :finance_count => data_array[4],
        :unknown_status_count => data_array[5],
        :executed_status_count => data_array[6],
        :in_progress_status_count => data_array[7],
        :not_executed_status_count => data_array[8]
    }

  end 


  def self.export_all
    audit_reports = "
      SELECT 
        ar.id, 
        ar.year,
        ar.title, 
        ar.text, 
        ar.period, 
        ar.control_organization_id, 
        ar.verified_organization_id, 
        ar.audit_type_id, 
        aut.name as audit_type_name,
        ar.arrival_date, 
        ar.assignment_date, 
        ar.received_date, 
        ar.confirmation_date, 
        ar.completion_date, 
        ar.user_id, 
        u.email as user_email,
		    u.first_name as user_first_name,
		    u.last_name as user_last_name,
        ar.sensitivity,
        vo.name as verified_organization_name,
        vo.abbreviation as verified_organization_abbreviation,
        co.name as control_organization_name,
        co.abbreviation as control_organization_abbreviation,
        vm.id as verified_ministry_id,
        vm.name as verified_ministry_name,
        vm.abbreviation as verified_ministry_abbreviation,
        cm.id as control_ministry_id,
        cm.name as control_ministry_name,
        cm.abbreviation as control_ministry_abbreviation,
        r.id as recommendation_id,
        r.title as recommendation_title,
        r.text as recommendation_text,
        r.reason,
        r.cost,
        r.recovered,
        r.justification,
        r.response, 
        rs.id as status_id,
        rs.name as status,
        e.id as evidence_id,
		    e.title as evidence_title,
		    e.text as evidence_text,
		    cr.category_id,
		    c.name as category_name
      FROM
        audit_reports ar
      INNER JOIN audit_types aut ON ar.audit_type_id = aut.id
      INNER JOIN users u ON ar.user_id = u.id
      INNER JOIN organizations vo ON ar.verified_organization_id = vo.id
      INNER JOIN organizations co ON ar.control_organization_id = co.id
      INNER JOIN ministries vm ON vo.ministry_id = vm.id
      INNER JOIN ministries cm ON co.ministry_id = cm.id
      LEFT JOIN recommendations r ON r.audit_report_id = ar.id
      LEFT JOIN recommendation_statuses rs ON rs.id = r.recommendation_status_id
      LEFT JOIN evidences e on e.id = r.evidence_id
	    LEFT JOIN categories_recommendations cr ON r.id = cr.recommendation_id
	    LEFT JOIN categories c ON c.id = cr.category_id
	    ORDER BY ar.id, r.id, e.id, c.id
      "

    audit_reports = AuditReport.find_by_sql(audit_reports)

    CSV.generate do |csv|
      csv << [I18n.t('activerecord.attributes.audit_report.id'), 
              I18n.t('activerecord.attributes.audit_report.year'), 
              I18n.t('activerecord.attributes.audit_report.title'),
              I18n.t('activerecord.attributes.audit_report.text'),
              I18n.t('activerecord.attributes.audit_report.period'),
              I18n.t('activerecord.attributes.audit_report.control_organization') + ' ID',
              I18n.t('activerecord.attributes.audit_report.verified_organization') + ' ID',
              I18n.t('activerecord.attributes.audit_report.audit_type')  + ' ID',
              I18n.t('activerecord.attributes.audit_report.audit_type'),
              I18n.t('activerecord.attributes.audit_report.arrival_date'),
              I18n.t('activerecord.attributes.audit_report.assignment_date'),
              I18n.t('activerecord.attributes.audit_report.received_date'),
              I18n.t('activerecord.attributes.audit_report.confirmation_date'),
              I18n.t('activerecord.attributes.audit_report.completion_date'),
              I18n.t('activerecord.models.user') + ' ID',
              I18n.t('activerecord.attributes.user.email'),
              I18n.t('activerecord.attributes.user.first_name'),
              I18n.t('activerecord.attributes.user.last_name'),
              I18n.t('activerecord.attributes.audit_report.sensitivity'),
              I18n.t('activerecord.attributes.audit_report.verified_organization'),
              I18n.t('activerecord.attributes.audit_report.verified_organization_abbreviation'),
              I18n.t('activerecord.attributes.audit_report.control_organization'),
              I18n.t('activerecord.attributes.audit_report.control_organization_abbreviation'),
              I18n.t('activerecord.attributes.audit_report.verified_ministry_id'),
              I18n.t('activerecord.attributes.audit_report.verified_ministry_name'),
              I18n.t('activerecord.attributes.audit_report.verified_ministry_abbreviation'),
              I18n.t('activerecord.attributes.audit_report.control_ministry_id'),
              I18n.t('activerecord.attributes.audit_report.control_ministry_name'),
              I18n.t('activerecord.attributes.audit_report.control_ministry_abbreviation'),
              I18n.t('activerecord.attributes.recommendation.id'),
              I18n.t('activerecord.attributes.recommendation.title'),
              I18n.t('activerecord.attributes.recommendation.text'),
              I18n.t('activerecord.attributes.recommendation.reason'),
              I18n.t('activerecord.attributes.recommendation.cost'),
              I18n.t('activerecord.attributes.recommendation.recovered'),
              I18n.t('activerecord.attributes.recommendation.justification'),
              I18n.t('activerecord.attributes.recommendation.response'),
              I18n.t('activerecord.attributes.recommendation.status') + ' ID',
              I18n.t('activerecord.attributes.recommendation.status'),
              I18n.t('activerecord.attributes.evidence.id'),
              I18n.t('activerecord.attributes.evidence.title'),
              I18n.t('activerecord.attributes.evidence.text'),
              I18n.t('activerecord.attributes.category.id'),
              I18n.t('activerecord.attributes.category.name')]
      audit_reports.each do |ar|
        csv << [ar.id, 
                ar.year,
                ar.title, 
                ar.text, 
                ar.period, 
                ar.control_organization_id, 
                ar.verified_organization_id, 
                ar.audit_type_id, 
                ar.audit_type_name, 
                ar.arrival_date, 
                ar.assignment_date, 
                ar.received_date, 
                ar.confirmation_date, 
                ar.completion_date, 
                ar.user_id, 
                ar.user_email,
                ar.user_first_name,
                ar.user_last_name,
                ar.sensitivity,
                ar.verified_organization_name,
                ar.verified_organization_abbreviation,
                ar.control_organization_name,
                ar.control_organization_abbreviation,
                ar.verified_ministry_id,
                ar.verified_ministry_name,
                ar.verified_ministry_abbreviation,
                ar.control_ministry_id,
                ar.control_ministry_name,
                ar.control_ministry_abbreviation,
                ar.recommendation_id,
                ar.recommendation_title,
                ar.recommendation_text,
                ar.reason,
                ar.cost,
                ar.recovered,
                ar.justification,
                ar.response,
                ar.status_id,
                ar.status,
                ar.evidence_id,
                ar.evidence_title,
                ar.evidence_text,
                ar.category_id,
                ar.category_name]
      end
    end 
  end
  def self.import(file)
    accessible_attributes = ['id','year','title', 'text', 'period', 'control_organization_id','verified_organization_id','audit_type_id','arrival_date','assignment_date','received_date','confirmation_date', 'completion_date','user_id']
    spreadsheet =Roo::Spreadsheet.open(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      audit = find_by_id(row["id"]) || new
      audit.attributes = row.to_hash.slice(*accessible_attributes)
      audit.save
    end

  end

 

end
