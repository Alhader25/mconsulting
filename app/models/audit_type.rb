class AuditType < ApplicationRecord
    has_many :audit_reports, dependent: :restrict_with_error
    validates :name, presence: true
    validates_uniqueness_of :name,:message=>"Erreur de doublon"
    def self.to_option_values
      ats = self.order(:name)
      audit_types = Hash.new
      
      ats.each do |at|
        audit_types[at.name] = at.id
      end
      return audit_types
    end
end

