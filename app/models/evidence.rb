class Evidence < ApplicationRecord
  belongs_to :audit_report
  has_many :recommendations, dependent: :nullify

  validates :title, presence: true
  validates_uniqueness_of :title,:message=>"Erreur de doublon"

  def self.import(file,report_id)
    xlsx = Roo::Spreadsheet.open(file.path)
    xlsx.each do |row|
        e=Evidence.new
        e.title=row[0]
        e.audit_report_id=report_id
         e.save!
    end
  end

end
