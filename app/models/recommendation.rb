require 'csv'
class Recommendation < ApplicationRecord
  scope :year, -> (year) { joins('INNER JOIN audit_reports ON audit_reports.id = recommendations.audit_report_id').where('audit_reports.year = ' + year)  }
  scope :sensitivity, -> (sensitivity) { joins(:audit_report).where(audit_reports: {sensitivity: sensitivity})  }
  scope :user, -> (user_id) { joins('INNER JOIN audit_reports ON audit_reports.id = recommendations.audit_report_id').where('audit_reports.user_id = ' + user_id) }
  scope :control, -> (control_organization_id) { joins('INNER JOIN audit_reports ON audit_reports.id = recommendations.audit_report_id').where('audit_reports.control_organization_id = ' + control_organization_id) }
  scope :verified, -> (verified_organization_id) { joins('INNER JOIN audit_reports ON audit_reports.id = recommendations.audit_report_id').where('audit_reports.verified_organization_id = ' + verified_organization_id) }
  #scope :status, -> (status) { where("recommendations.status = '" + status + "'" ) }
  scope :ministry, -> (ministry_id) { joins('INNER JOIN organizations ON audit_reports.verified_organization_id = organizations.id').where('organizations.ministry_id = ' + ministry_id) }
  scope :status, -> (status_id) { joins("INNER JOIN recommendation_statuses ON recommendations.recommendation_status_id = recommendation_statuses.id").where("recommendation_statuses.id = '" + status_id + "'" ) }
  scope :report, -> (report) { joins('INNER JOIN audit_reports ON audit_reports.id = recommendations.audit_report_id').where('audit_reports.id = ' + report) }

  include Filterable
  #using postgreSQL enum type: https://www.sitepoint.com/enumerated-types-with-activerecord-and-postgresql/
  enum status: {
      complete:  'complete',
      incomplete: 'incomplete',
      ongoing: 'ongoing',
      unknown: 'unknown'
  }

  belongs_to :audit_report
  belongs_to :evidence, optional: true
  has_and_belongs_to_many :categories
  belongs_to :recommendation_status
  validates_uniqueness_of :title, scope: :audit_report
  validates :title, presence: true
  validates :category_ids, presence: true
  #validates :text, presence: true

  def self.filtered_recommendations(filter_params, to_csv=false)
      recommendations = Recommendation
        .joins(:recommendation_status, audit_report: [:control_organization, {verified_organization: :ministry}])
        .includes(:recommendation_status, audit_report: [:control_organization, {verified_organization: :ministry}])
        .filters(filter_params)
        .order("audit_reports.year", "ministries.name", "verified_organizations_audit_reports.name", "organizations.name")

      if to_csv
        CSV.generate do |csv|
          csv << [I18n.t('activerecord.attributes.ministry.name'), I18n.t('activerecord.attributes.audit_report.verified_organization'), I18n.t('activerecord.attributes.audit_report.control_organization'), I18n.t('activerecord.attributes.recommendation.year'), I18n.t('activerecord.attributes.recommendation.title'), I18n.t('activerecord.attributes.recommendation.status'), I18n.t('activerecord.attributes.recommendation.cost'), I18n.t("activerecord.attributes.recommendation.recovered")]
          recommendations.each do |r|
            csv << [r.audit_report.verified_organization.ministry.name, r.audit_report.verified_organization.name, r.audit_report.control_organization.name, r.audit_report.year, r.title, r.recommendation_status.name, r.cost, r.recovered]
          end
        end
      else 
        return recommendations
      end 
  end
  #===== REPORT FUNCTIONS ======
  def self.recommendations_with_categories(year, status, to_csv=false, to_summary=false)
    recommendations = "
      WITH root_categories AS (
        SELECT  
          c2.id AS category_id, 
          c1.id AS root_category_id, 
          c2.name AS category_name, 
          c1.name AS root_category_name
        FROM public.categories c1 
        INNER JOIN public.categories c2 ON c2.ancestry = c1.id::text OR c2.ancestry LIKE c1.id::text || '/%' 
        UNION
        SELECT 
          id AS category_id, 
          id AS root_category_id, 
          name AS category_name, 
          name AS root_category_name
        FROM public.categories 
        WHERE ancestry is null
      )
      SELECT DISTINCT 
        r.id, 
        ar.year, 
        r.title, 
        r.cost, 
        r.recovered, 
        rs.name as recommendation_status_name, 
        rc.root_category_name, 
        rc.root_category_id, 
        rc.category_name,
		    rc.category_id,
        r.audit_report_id, 
        r.evidence_id,
		    vo.name as verified_organization,
        co.name as control_organization,
        vm.name as verified_ministry
      FROM recommendations r 
      INNER JOIN recommendation_statuses rs ON rs.id = r.recommendation_status_id
      INNER JOIN audit_reports ar ON r.audit_report_id = ar.id
      INNER JOIN organizations vo ON ar.verified_organization_id = vo.id
      INNER JOIN organizations co ON ar.control_organization_id = co.id
      INNER JOIN ministries vm ON vo.ministry_id = vm.id
      INNER JOIN categories_recommendations cr ON cr.recommendation_id = r.id
      INNER JOIN root_categories rc ON rc.category_id = cr.category_id
      WHERE 0=0
      "
      recommendations = recommendations + " AND ar.year= " + year if !year.nil? && year.length > 0
      recommendations = recommendations + " AND rs.id= " + status  if !status.nil? && status.length > 0
      recommendations = recommendations + "
      ORDER BY audit_report_id, id, root_category_id
      "

    recommendations = Recommendation.find_by_sql(recommendations)

    if to_csv
      CSV.generate do |csv|
        csv << [I18n.t('activerecord.attributes.recommendation.title'), I18n.t('activerecord.attributes.audit_report.year'), I18n.t('activerecord.attributes.recommendation.cost'), I18n.t("activerecord.attributes.recommendation.recovered"), I18n.t('activerecord.attributes.recommendation.status'), I18n.t("activerecord.models.category"), I18n.t("activerecord.models.category"),I18n.t('activerecord.attributes.audit_report.control_organization'),I18n.t('activerecord.models.ministry'),I18n.t('activerecord.attributes.audit_report.verified_organization')]
        recommendations.each do |r|
          csv << [r.title, r.year, r.cost, r.recovered, r.recommendation_status_name, r.root_category_name, r.category_name,r.control_organization,r.verified_ministry,r.verified_organization]
        end
      end
    elsif to_summary
      cats = {}
      total_cost = 0
      total_recovered = 0
      total_count = 0
      recommendations.each do |c|
        !c.cost ? cost = 0 : cost = c.cost.to_i
        !c.recovered ? recovered = 0 : recovered = c.recovered.to_i
        if !cats[c.category_name]
          cats[c.category_name] = {:count => 1,:cost => cost,:recovered => recovered} 
        else
          cats[c.category_name][:count] = cats[c.category_name][:count] + 1
          cats[c.category_name][:cost] = cats[c.category_name][:cost] + cost
          cats[c.category_name][:recovered] = cats[c.category_name][:recovered] + recovered
        end
        total_cost = total_cost + cost
        total_recovered = total_recovered + recovered
        total_count = total_count + 1
      end
      return {:categories => cats, :total_cost => total_cost, :total_recovered => total_recovered, :total_count => total_count}

    else 
      return recommendations
    end 
  end

  def self.import(file,report_id)
    xlsx = Roo::Spreadsheet.open(file.path)
    xlsx.each do |row|
        r=Recommendation.new
        r.title=row[0]
        r.text=row[1]
        r.cost=row[2]
        r.recovered=row[3]
        r.recommendation_status_id=row[4]
        if row[5].present?
          #r.evidence_id=row[5]
        end
        r.justification=row[6]
        r.response=row[7]
        r.category_ids=3
        r.audit_report_id=report_id
        r.save!
    end
  end
end
