// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require jquery3
//= require popper
//= require bootstrap-sprockets
//= require moment
//= require moment/fr
//= require tempusdominus-bootstrap-4
//= require chosen-jquery
//= require_tree .
//= require twitter/typeahead

//enable alert dismissal
$('.alert').alert()

//enable popovers
$(function () {
  $('[data-toggle="popover"]').popover()
})

//enable tooltips
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

// datetimepicker set by class to use for all date inputs
$(function () {
  $('.datepicker').datetimepicker({
      format: 'YYYY-MM-DD',
      locale: 'fr',
      tooltips: dateTips
  });
});

// tooltips in French for datetimepicker
const dateTips = {
  today: 'Revenir à aujourd\'hui',
  clear: 'Effacer la sélection',
  close: 'Fermer',
  selectMonth: 'Sélectionner le mois',
  prevMonth: 'Mois précédent',
  nextMonth: 'Mois suivant',
  selectYear: 'Sélectionner l\'année',
  prevYear: 'Année précédente',
  nextYear: 'Année suivante',
  selectDecade: 'Sélectionner la décénnie',
  prevDecade: 'Décénnie précédente',
  nextDecade: 'Décennie suivante',
  prevCentury: 'Siècle précédent',
  nextCentury: 'Siècle suivant'
};

// enable chosen select box
$(function(){
  $('.chosen-select').chosen({
    inherit_select_classes: true,
    allow_single_deselect: true,
    width: '100%',
    create_option: true,
    persistent_create_option: true
  })
});



// Export Word
function export_to_word() {
   var link, blob, url;
   blob = new Blob(['\ufeff', document.getElementById("docx").innerHTML], {
         type: 'application/msword'
   });
   url = URL.createObjectURL(blob);
   link = document.createElement('A');
   link.href = url;
   link.download = 'Document';  // default name without extension 
   document.body.appendChild(link);
   if (navigator.msSaveOrOpenBlob )
        navigator.msSaveOrOpenBlob( blob, 'Document.doc'); // IE10-11
   else link.click();  // other browsers
   document.body.removeChild(link);
 };

var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name) {
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    window.location.href = uri + base64(format(template, ctx))
  }
})()