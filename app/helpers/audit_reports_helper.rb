module AuditReportsHelper
    def localized_sensitivities
      Hash[AuditReport.sensitivities.map { |k,v| [AuditReport.human_attribute_name("sensitivity.#{k}"), v] }]
    end
end
