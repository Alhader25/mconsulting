module RecommendationsHelper
    def localized_statuses
        Hash[Recommendation.statuses.map { |k,v| [Recommendation.human_attribute_name("status.#{k}"), k] }]
    end
end
