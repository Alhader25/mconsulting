module UsersHelper
  def localized_roles
    Hash[User.roles.map { |k,v| [User.human_attribute_name("role.#{k}"), v] }]
  end
end
