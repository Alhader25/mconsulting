require 'rails_helper'

RSpec.describe Organization, type: :model do
  describe 'associations' do
    it { should have_many(:control_audit_reports).class_name('AuditReport')  }
    it { should have_many(:verified_audit_reports).class_name('AuditReport')  }
    it { should have_many(:users) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }

    it 'is invalid without a name' do
      expect(FactoryBot.build(:organization, name: nil)).not_to be_valid
    end
  end

  describe 'instance methods' do
    it 'returns a hash of organizations' do
      o4 = FactoryBot.create(:organization, name: 'org4', abbreviation: 'o4')
      o2 = FactoryBot.create(:organization, name: 'org2', abbreviation: 'o2')
      o3 = FactoryBot.create(:organization, name: 'org3', abbreviation: 'o3')
      o1 = FactoryBot.create(:organization, name: 'org1', abbreviation: 'o1')
      organization_hash = Organization.to_option_values

      expect(organization_hash).to be_a(Hash)   
      expect(organization_hash.size).to eq(4)
      expect(organization_hash["o1: org1"]).to eq(o1.id)
      expect(organization_hash["o2: org2"]).to eq(o2.id)
      expect(organization_hash["o3: org3"]).to eq(o3.id)
      expect(organization_hash["o4: org4"]).to eq(o4.id)
    end 
  end
end