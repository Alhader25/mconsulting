require 'rails_helper'

RSpec.describe Recommendation, type: :model do
  describe 'associations' do
    it { should belong_to(:audit_report) }
    it { should have_and_belong_to_many(:categories) }
  end

  describe 'validations' do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:text) }

    it 'is invalid without a title' do
      expect(FactoryBot.build(:recommendation, title: nil)).not_to be_valid
    end

    it 'is invalid without a text' do
      expect(FactoryBot.build(:recommendation, text: nil)).not_to be_valid
    end

  end
end