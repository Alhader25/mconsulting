require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'associations' do
    it { should belong_to(:organization) }
    it { should have_many(:audit_reports) }
  end

  describe 'validations' do
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:last_name) }

    it 'is invalid without a first_name' do
      expect(FactoryBot.build(:user, first_name: nil)).not_to be_valid
    end

    it 'is invalid without a last_name' do
      expect(FactoryBot.build(:user, last_name: nil)).not_to be_valid
    end

    it 'is invalid without an email' do
      expect(FactoryBot.build(:user, email: nil)).not_to be_valid
    end
  end

  describe 'instance methods' do
    it 'returns a hash of users' do
      u2 = FactoryBot.create(:user, email: 'user2@user.com')
      u1 = FactoryBot.create(:user, email: 'user1@user.com')
      user_hash = User.to_option_values

      expect(user_hash).to be_a(Hash)   
      expect(user_hash.size).to eq(2)
      expect(user_hash["user1@user.com"]).to eq(u1.id)
      expect(user_hash["user2@user.com"]).to eq(u2.id)
    end 
  end
end