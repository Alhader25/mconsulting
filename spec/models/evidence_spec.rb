require 'rails_helper'

RSpec.describe Evidence, type: :model do
  describe 'associations' do
    it { should have_many(:recommendations) }
    it { should belong_to(:audit_report) }
  end

  describe 'validations' do
    it { should validate_presence_of(:title) }

    it 'is invalid without a name' do
      expect(FactoryBot.build(:evidence, title: nil)).not_to be_valid
    end
  end
end
