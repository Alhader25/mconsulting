require 'rails_helper'

RSpec.describe AuditReport, type: :model do
  describe 'associations' do
    it { should belong_to(:control_organization).class_name('Organization') }
    it { should belong_to(:verified_organization).class_name('Organization') }
    it { should belong_to(:audit_type) }
    it { should belong_to(:user) }
    it { should have_many(:recommendations) }
  end

  describe 'validations' do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:year) }

    it "is invalid without a title" do
      FactoryBot.build(:audit_report, title: nil).should_not be_valid
    end

    it "is invalid without a year" do
      FactoryBot.build(:audit_report, year: nil).should_not be_valid
    end

  end
end