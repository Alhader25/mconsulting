require 'rails_helper'

RSpec.describe Category, type: :model do
  describe 'associations' do
    it { should have_and_belong_to_many(:recommendations) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
  end

  describe 'ancestry' do
    before(:each) do
      @root = FactoryBot.create(:category, name: 'root node')
      @leaf11 = FactoryBot.create(:category, name: 'first leaf')
      @leaf11.parent = @root
      @leaf11.save
      @leaf12 = FactoryBot.create(:category, name: 'second leaf')
      @leaf12.parent = @root
      @leaf12.save
      @leaf311 = FactoryBot.create(:category, name: 'leaf of leaf')
      @leaf311.parent = @leaf11
      @leaf311.save
    end
    
    it 'creates a hierarchy' do
      expect(@root.is_root?).to eq(true)
      expect(@leaf11.is_root?).to eq(false)
      expect(@leaf12.is_root?).to eq(false)   
      expect(@leaf311.is_root?).to eq(false)   

      expect(@leaf11.parent).to eq(@root)
      expect(@leaf12.parent).to eq(@root)
      expect(@leaf311.parent).to eq(@leaf11)
    end

    it 'arranges nodes as array with class method arrange_as_array' do 
      cat_array = Category.arrange_as_array
      expect(cat_array).to be_a(Array)
      expect(cat_array[0]).to eq(@root)
      expect(cat_array[1]).to eq(@leaf11)
      expect(cat_array[2]).to eq(@leaf311)
      expect(cat_array[3]).to eq(@leaf12)
    end 

    it 'outputs names for selects using instance method name_for_selects' do
      expect(@root.name_for_selects).to eq(' root node')
      expect(@leaf11.name_for_selects).to eq('-- first leaf')
      expect(@leaf12.name_for_selects).to eq('-- second leaf')
      expect(@leaf311.name_for_selects).to eq('---- leaf of leaf')
    end

    it 'outputs possible parent using instance method possible_parents' do
      expect(@root.possible_parents).to be_a(Array)
      expect(@root.possible_parents.length).to eq(0)
      expect(@leaf11.possible_parents).to be_a(Array)
      expect(@leaf11.possible_parents.length).to eq(2)
      expect(@leaf12.possible_parents).to be_a(Array)
      expect(@leaf12.possible_parents.length).to eq(3)
      expect(@leaf311.possible_parents).to be_a(Array)
      expect(@leaf311.possible_parents.length).to eq(3)
    end
  end
end