require 'rails_helper'

RSpec.describe Region, type: :model do
  describe 'associations' do
    it { should have_many(:organizations) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }

    it 'is invalid without a name' do
      expect(FactoryBot.build(:region, name: nil)).not_to be_valid
    end
  end
end
