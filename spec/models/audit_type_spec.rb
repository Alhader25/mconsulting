require 'rails_helper'

RSpec.describe AuditType, type: :model do
  describe 'associations' do
    it { should have_many(:audit_reports) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }

    it 'is invalid without a name' do
      expect(FactoryBot.build(:audit_type, name: nil)).not_to be_valid
    end
  end

  describe 'instance methods' do
    it 'returns a hash of audit types' do
      at1 = FactoryBot.create(:audit_type, name: 'type1')
      at2 = FactoryBot.create(:audit_type, name: 'type2')
      at3 = FactoryBot.create(:audit_type, name: 'type3')
      audit_hash = AuditType.to_option_values

      expect(audit_hash).to be_a(Hash)   
      expect(audit_hash.size).to eq(3)
      expect(audit_hash["type1"]).to eq(at1.id)
      expect(audit_hash["type2"]).to eq(at2.id)
      expect(audit_hash["type3"]).to eq(at3.id)
    end 
  end
end