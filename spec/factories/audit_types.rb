FactoryBot.define do
  factory :audit_type do
    name  { ['Financial', 'Performance'].sample }
  end
end