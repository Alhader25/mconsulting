FactoryBot.define do
  factory :ministry do
    name { Faker::Company.name }
    abbreviation { Faker::Name.initials }
  end
end
