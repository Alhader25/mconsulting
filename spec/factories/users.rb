FactoryBot.define do
  factory :user do
    trait :admin do
      add_attribute(:role) { 'admin' }
    end
    email         { Faker::Internet.email }
    password  { Faker::Internet.password }
    confirmed_at  { Faker::Date.between(from: 1.year.ago, to: 1.day.ago) }
    first_name  { Faker::Name.first_name  }
    last_name  { Faker::Name.last_name  }
    add_attribute(:role) { 'user' }
    organization

    factory :user_with_data do
      # audit_reports_count is declared as a transient attribute and available in
      # attributes on the factory, as well as the callback via the evaluator
      transient do
        audit_reports_count { 3 }
      end

      # the after(:create) yields two values; the user instance itself and the
      # evaluator, which stores all values from the factory, including transient
      # attributes; `create_list`'s second argument is the number of records
      # to create and we make sure the user is associated properly to the 
      # audit_report
      after(:create) do |user, evaluator|
        create_list(:audit_report, evaluator.audit_reports_count, user: user)
      end
    end
  end
end