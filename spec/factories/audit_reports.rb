FactoryBot.define do
  factory :audit_report do
    year     { Faker::Number.within(range: 2009..2021) }
    title         { Faker::Hipster.sentence }
    text  { Faker::Hipster.paragraph }
    period       { Faker::Number.within(range: 1..5) }
    arrival_date  { Faker::Date.between(from: 1.year.ago, to: Date.today) }
    assignment_date   { Faker::Date.between(from: 1.year.ago, to: Date.today) }
    received_date   { Faker::Date.between(from: 1.year.ago, to: Date.today) }
    confirmation_date   { Faker::Date.between(from: 1.year.ago, to: Date.today) }
    completion_date    { Faker::Date.between(from: 1.year.ago, to: Date.today) }
    sensitivity  { ['high', 'low'].sample }
    user
    audit_type
    association :control_organization, factory: [:organization, :control]
    association :verified_organization, factory: :organization

    factory :audit_report_with_data do
      # recommendations_count is declared as a transient attribute and available in
      # attributes on the factory, as well as the callback via the evaluator
      transient do
        recommendations_count { 5 }
      end

      # the after(:create) yields two values; the audit report instance itself and the
      # evaluator, which stores all values from the factory, including transient
      # attributes; `create_list`'s second argument is the number of records
      # to create and we make sure the audit_report is associated properly to the 
      # recommendation 
      after(:create) do |audit_report, evaluator|
        create_list(:recommendation, evaluator.recommendations_count, audit_report: audit_report)
      end
    end
  end
end