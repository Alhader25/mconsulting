FactoryBot.define do
  factory :recommendation do
    title         { Faker::Hipster.sentence }
    text  { Faker::Hipster.paragraph }
    reason  { Faker::Hipster.paragraph }
    justification  { Faker::Hipster.paragraph }
    response  { Faker::Hipster.paragraph }
    cost       { Faker::Number.decimal(5) }
    recovered       { Faker::Number.decimal(5) }
    status  { ['complete', 'incomplete', 'ongoing', 'unknown'].sample }
    audit_report
    recommendation_status

    transient do
      categories_count { 3 }
    end

    after(:create) do |recommendation, evaluator|
      create_list(:category, evaluator.categories_count, recommendations: [recommendation])
    end
  end
end