FactoryBot.define do
  factory :recommendation_status do
    name { "Executed" }
  end
end
