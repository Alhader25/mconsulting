FactoryBot.define do
  factory :organization do
    trait :control do
      add_attribute(:control) { true }
    end
    name  { Faker::Company.name }
    abbreviation { Faker::Name.initials }
    add_attribute(:control) { false }
    ministry
    region
  end
end