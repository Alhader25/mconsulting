FactoryBot.define do
  factory :evidence do
    title { Faker::Hipster.sentence }
    text { Faker::Hipster.paragraph }
    audit_report
  end
end
