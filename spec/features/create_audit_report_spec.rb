require 'rails_helper'
 
RSpec.describe 'Creating an audit report', type: :feature do
  scenario 'create starting at audit report list' do
    FactoryBot.create(:audit_type)
    FactoryBot.create(:organization, :control)
    user = FactoryBot.create(:user, :admin)
    login_as(user, :scope => :user)
    visit audit_reports_path
    click_on 'addAuditReport'
    #fill_in 'audit_report_title', with: 'Test Audit Report'
    #fill_in 'audit_report_text', with: 'Test Audit Report Text'
    #click_on 'createAuditReport'
    #visit audit_reports_path
    expect(page).to have_content("Nouveau rapport d'audit")
  end
end