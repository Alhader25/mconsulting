require 'rails_helper'
 
RSpec.describe 'Viewing audit reports list, filter and main layout menu', type: :feature do
  scenario 'valid elements for admin user' do
    user = FactoryBot.create(:user, :admin)
    audit_report = FactoryBot.create(:audit_report)
    login_as(user, :scope => :user)
    visit audit_reports_path
    #report list table
    expect(page).to have_css('table#auditReportsList')
    expect(page).to have_css('a#addAuditReport')
    expect(page).to have_content(audit_report.title)
    expect(page).to have_selector("a[href='/audit_reports/#{audit_report.id}/edit']")
    #filter
    expect(page).to have_select('user')
    expect(page).to have_select('control')
    expect(page).to have_select('year')
    expect(page).to have_select('verified')
    #Admin menu
    expect(page).to have_css('a#userProfile')
    expect(page).to have_css('a#userManagement')
    expect(page).to have_css('a#auditTypeManagement')
    expect(page).to have_css('a#categoryManagement')
    expect(page).to have_css('a#organizationManagement')
    expect(page).to have_css('a#logout')
  end

  scenario 'valid elements for regular user' do
    user = FactoryBot.create(:user)
    audit_report = FactoryBot.create(:audit_report)
    login_as(user, :scope => :user)
    visit audit_reports_path
    #report list table
    expect(page).to have_css('table#auditReportsList')
    expect(page).not_to have_css('a#addAuditReport')
    expect(page).not_to have_content(audit_report.title)
    expect(page).not_to have_selector("a[href='/audit_reports/#{audit_report.id}/edit']")
    #filter
    expect(page).not_to have_select('user')
    expect(page).to have_select('control')
    expect(page).to have_select('year')
    expect(page).to have_select('verified')
    #Admin menu
    expect(page).to have_css('a#userProfile')
    expect(page).not_to have_css('a#userManagement')
    expect(page).not_to have_css('a#auditTypeManagement')
    expect(page).not_to have_css('a#categoryManagement')
    expect(page).not_to have_css('a#organizationManagement')
    expect(page).to have_css('a#logout')
  end
end