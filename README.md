# README

This application can be run using Docker and docker-compose (https://www.docker.com/).

Three environment variables are needed for the production database connection:

    POSTGRES_HOST
    POSTGRES_USER
    POSTGRES_PASSWORD


To run the application locally using docker, install docker and run the following commands from the application root directory:

    docker-compose build

    docker-compose run web rails db:setup

    docker-compose run web rails webpacker:install

    docker-compose up