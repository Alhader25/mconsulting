require 'csv'

#audit types (to be updated)
AuditType.create([{name: 'Performance'},  {name: 'Financier'}])

#statuses (to be updated)
RecommendationStatus.create([{name: 'Aucune Réponse'},  {name: 'En Cours'}, {name: 'Exécutée'},  {name: 'Non Exécutée'}])

#regions (to be updated)
Region.create([{name: 'Bamako'}])

#ministries
CSV.foreach(Rails.root.join('db/ministry_seeds.csv'), headers: false) do |row|
  Ministry.create({name: row[0], abbreviation: row[1]})
end

#control organizations
CSV.foreach(Rails.root.join('db/control_organization_seeds.csv'), headers: false) do |row|
  ministry = Ministry.find_or_create_by(:name => row[0])
  org = Organization.create({name: row[2], abbreviation: row[1], control: true, region_id: 1})
  org.ministry = ministry
  org.save!
end

#other (verified) organizations
CSV.foreach(Rails.root.join('db/non-control_organization_seeds.csv'), headers: false) do |row|
  ministry = Ministry.find_or_create_by(:name => row[0])
  org = Organization.create({name: row[3], abbreviation: row[2], control: false, region_id: 1})
  org.ministry = ministry
  org.save!
end

#categories
CSV.foreach(Rails.root.join('db/category_seeds.csv'), headers: false) do |row|
  if row[1]
    Category.create :name => row[0], :parent => Category.find_by({name: row[1]})
  else 
    Category.create({name: row[0]})
  end 
end

#base users
cgsp = Organization.find_by({abbreviation: 'CGSP'})
User.create(email: 'ims@msidevcloud.com', password: 'sngpcgsp', role: 'admin', confirmed_at: Time.now, organization_id: cgsp.id, first_name: 'ims', last_name: 'user')
User.create(email: 'admin@sngp-cgsp.com', password: 'sngpcgsp', role: 'admin', confirmed_at: Time.now, organization_id: cgsp.id, first_name: 'admin', last_name: 'user')
User.create(email: 'user1@sngp-cgsp.com', password: 'sngpcgsp', role: 'user', confirmed_at: Time.now, organization_id: cgsp.id, first_name: 'user1', last_name: 'user')
User.create(email: 'user2@sngp-cgsp.com', password: 'sngpcgsp', role: 'user', confirmed_at: Time.now, organization_id: cgsp.id, first_name: 'user2', last_name: 'user')

namespace :deploy do
  desc "reload the database with seed data"
  task :seed do
    on roles(:all) do
      within current_path do
        execute :bundle, :exec, 'rails', 'db:seed', 'RAILS_ENV=production'
      end
    end
  end
end