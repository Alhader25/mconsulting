SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: app_role; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.app_role AS ENUM (
    'admin',
    'user',
    'restricted_user'
);


--
-- Name: recommendation_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.recommendation_status AS ENUM (
    'complete',
    'incomplete',
    'ongoing',
    'unknown'
);


--
-- Name: sensitivity; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.sensitivity AS ENUM (
    'high',
    'low'
);


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: audit_reports; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.audit_reports (
    id bigint NOT NULL,
    year integer,
    title text,
    text text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    control_organization_id integer NOT NULL,
    verified_organization_id integer NOT NULL,
    audit_type_id bigint,
    arrival_date date,
    assignment_date date,
    received_date date,
    confirmation_date date,
    completion_date date,
    user_id bigint,
    sensitivity public.sensitivity,
    reports json,
    recipient character varying,
    period character varying
);


--
-- Name: audit_reports_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.audit_reports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: audit_reports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.audit_reports_id_seq OWNED BY public.audit_reports.id;


--
-- Name: audit_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.audit_types (
    id bigint NOT NULL,
    name text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: audit_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.audit_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: audit_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.audit_types_id_seq OWNED BY public.audit_types.id;


--
-- Name: categories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.categories (
    id bigint NOT NULL,
    name text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    ancestry character varying
);


--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.categories_id_seq OWNED BY public.categories.id;


--
-- Name: categories_recommendations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.categories_recommendations (
    category_id bigint NOT NULL,
    recommendation_id bigint NOT NULL
);


--
-- Name: evidences; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.evidences (
    id bigint NOT NULL,
    title character varying,
    text text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    audit_report_id bigint
);


--
-- Name: evidences_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.evidences_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: evidences_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.evidences_id_seq OWNED BY public.evidences.id;


--
-- Name: ministries; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ministries (
    id bigint NOT NULL,
    name character varying,
    abbreviation character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: ministries_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.ministries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ministries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.ministries_id_seq OWNED BY public.ministries.id;


--
-- Name: organizations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.organizations (
    id bigint NOT NULL,
    name text,
    abbreviation text,
    control boolean DEFAULT false,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    ministry_id bigint,
    region_id bigint
);


--
-- Name: organizations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.organizations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: organizations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.organizations_id_seq OWNED BY public.organizations.id;


--
-- Name: recommendation_statuses; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.recommendation_statuses (
    id bigint NOT NULL,
    name character varying,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: recommendation_statuses_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.recommendation_statuses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: recommendation_statuses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.recommendation_statuses_id_seq OWNED BY public.recommendation_statuses.id;


--
-- Name: recommendations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.recommendations (
    id bigint NOT NULL,
    text text,
    title text,
    reason text,
    cost numeric,
    recovered numeric,
    justification text,
    response text,
    audit_report_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    status public.recommendation_status,
    evidence_id bigint,
    recommendation_status_id bigint
);


--
-- Name: recommendations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.recommendations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: recommendations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.recommendations_id_seq OWNED BY public.recommendations.id;


--
-- Name: regions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.regions (
    id bigint NOT NULL,
    name character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: regions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.regions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: regions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.regions_id_seq OWNED BY public.regions.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip inet,
    last_sign_in_ip inet,
    confirmation_token character varying,
    confirmed_at timestamp without time zone,
    confirmation_sent_at timestamp without time zone,
    unconfirmed_email character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    role public.app_role,
    first_name text,
    last_name text,
    organization_id bigint
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: audit_reports id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.audit_reports ALTER COLUMN id SET DEFAULT nextval('public.audit_reports_id_seq'::regclass);


--
-- Name: audit_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.audit_types ALTER COLUMN id SET DEFAULT nextval('public.audit_types_id_seq'::regclass);


--
-- Name: categories id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.categories ALTER COLUMN id SET DEFAULT nextval('public.categories_id_seq'::regclass);


--
-- Name: evidences id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.evidences ALTER COLUMN id SET DEFAULT nextval('public.evidences_id_seq'::regclass);


--
-- Name: ministries id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ministries ALTER COLUMN id SET DEFAULT nextval('public.ministries_id_seq'::regclass);


--
-- Name: organizations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.organizations ALTER COLUMN id SET DEFAULT nextval('public.organizations_id_seq'::regclass);


--
-- Name: recommendation_statuses id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.recommendation_statuses ALTER COLUMN id SET DEFAULT nextval('public.recommendation_statuses_id_seq'::regclass);


--
-- Name: recommendations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.recommendations ALTER COLUMN id SET DEFAULT nextval('public.recommendations_id_seq'::regclass);


--
-- Name: regions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.regions ALTER COLUMN id SET DEFAULT nextval('public.regions_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: audit_reports audit_reports_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.audit_reports
    ADD CONSTRAINT audit_reports_pkey PRIMARY KEY (id);


--
-- Name: audit_types audit_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.audit_types
    ADD CONSTRAINT audit_types_pkey PRIMARY KEY (id);


--
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: evidences evidences_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.evidences
    ADD CONSTRAINT evidences_pkey PRIMARY KEY (id);


--
-- Name: ministries ministries_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ministries
    ADD CONSTRAINT ministries_pkey PRIMARY KEY (id);


--
-- Name: organizations organizations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.organizations
    ADD CONSTRAINT organizations_pkey PRIMARY KEY (id);


--
-- Name: recommendation_statuses recommendation_statuses_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.recommendation_statuses
    ADD CONSTRAINT recommendation_statuses_pkey PRIMARY KEY (id);


--
-- Name: recommendations recommendations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.recommendations
    ADD CONSTRAINT recommendations_pkey PRIMARY KEY (id);


--
-- Name: regions regions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.regions
    ADD CONSTRAINT regions_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_audit_reports_on_audit_type_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_audit_reports_on_audit_type_id ON public.audit_reports USING btree (audit_type_id);


--
-- Name: index_audit_reports_on_control_organization_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_audit_reports_on_control_organization_id ON public.audit_reports USING btree (control_organization_id);


--
-- Name: index_audit_reports_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_audit_reports_on_user_id ON public.audit_reports USING btree (user_id);


--
-- Name: index_audit_reports_on_verified_organization_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_audit_reports_on_verified_organization_id ON public.audit_reports USING btree (verified_organization_id);


--
-- Name: index_categories_on_ancestry; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_categories_on_ancestry ON public.categories USING btree (ancestry);


--
-- Name: index_categories_recommendations_on_category_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_categories_recommendations_on_category_id ON public.categories_recommendations USING btree (category_id);


--
-- Name: index_categories_recommendations_on_recommendation_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_categories_recommendations_on_recommendation_id ON public.categories_recommendations USING btree (recommendation_id);


--
-- Name: index_evidences_on_audit_report_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_evidences_on_audit_report_id ON public.evidences USING btree (audit_report_id);


--
-- Name: index_organizations_on_ministry_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_organizations_on_ministry_id ON public.organizations USING btree (ministry_id);


--
-- Name: index_organizations_on_region_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_organizations_on_region_id ON public.organizations USING btree (region_id);


--
-- Name: index_recommendations_on_audit_report_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_recommendations_on_audit_report_id ON public.recommendations USING btree (audit_report_id);


--
-- Name: index_recommendations_on_evidence_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_recommendations_on_evidence_id ON public.recommendations USING btree (evidence_id);


--
-- Name: index_recommendations_on_recommendation_status_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_recommendations_on_recommendation_status_id ON public.recommendations USING btree (recommendation_status_id);


--
-- Name: index_users_on_confirmation_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_confirmation_token ON public.users USING btree (confirmation_token);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_email ON public.users USING btree (email);


--
-- Name: index_users_on_organization_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_on_organization_id ON public.users USING btree (organization_id);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON public.users USING btree (reset_password_token);


--
-- Name: recommendations fk_rails_04ee78feb5; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.recommendations
    ADD CONSTRAINT fk_rails_04ee78feb5 FOREIGN KEY (recommendation_status_id) REFERENCES public.recommendation_statuses(id);


--
-- Name: audit_reports fk_rails_05aa76ccd0; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.audit_reports
    ADD CONSTRAINT fk_rails_05aa76ccd0 FOREIGN KEY (audit_type_id) REFERENCES public.audit_types(id);


--
-- Name: audit_reports fk_rails_21a6e74506; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.audit_reports
    ADD CONSTRAINT fk_rails_21a6e74506 FOREIGN KEY (control_organization_id) REFERENCES public.organizations(id);


--
-- Name: evidences fk_rails_267f4221ea; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.evidences
    ADD CONSTRAINT fk_rails_267f4221ea FOREIGN KEY (audit_report_id) REFERENCES public.audit_reports(id);


--
-- Name: categories_recommendations fk_rails_39281d24a1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.categories_recommendations
    ADD CONSTRAINT fk_rails_39281d24a1 FOREIGN KEY (recommendation_id) REFERENCES public.recommendations(id);


--
-- Name: audit_reports fk_rails_477919cf3b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.audit_reports
    ADD CONSTRAINT fk_rails_477919cf3b FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: recommendations fk_rails_6aa9fa1f1b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.recommendations
    ADD CONSTRAINT fk_rails_6aa9fa1f1b FOREIGN KEY (audit_report_id) REFERENCES public.audit_reports(id);


--
-- Name: audit_reports fk_rails_79992dd688; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.audit_reports
    ADD CONSTRAINT fk_rails_79992dd688 FOREIGN KEY (verified_organization_id) REFERENCES public.organizations(id);


--
-- Name: categories_recommendations fk_rails_8f1ad18679; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.categories_recommendations
    ADD CONSTRAINT fk_rails_8f1ad18679 FOREIGN KEY (category_id) REFERENCES public.categories(id);


--
-- Name: recommendations fk_rails_92d6dc38ee; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.recommendations
    ADD CONSTRAINT fk_rails_92d6dc38ee FOREIGN KEY (evidence_id) REFERENCES public.evidences(id);


--
-- Name: organizations fk_rails_b7f9c058e8; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.organizations
    ADD CONSTRAINT fk_rails_b7f9c058e8 FOREIGN KEY (region_id) REFERENCES public.regions(id);


--
-- Name: users fk_rails_d7b9ff90af; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT fk_rails_d7b9ff90af FOREIGN KEY (organization_id) REFERENCES public.organizations(id);


--
-- Name: organizations fk_rails_fc23f9052a; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.organizations
    ADD CONSTRAINT fk_rails_fc23f9052a FOREIGN KEY (ministry_id) REFERENCES public.ministries(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO "schema_migrations" (version) VALUES
('20190606224754'),
('20190607183833'),
('20190610183355'),
('20190610183950'),
('20190611170649'),
('20190612145736'),
('20190612151248'),
('20190612181441'),
('20190612181521'),
('20190612185043'),
('20190613125342'),
('20190613142905'),
('20190613183053'),
('20190614172926'),
('20190614174837'),
('20190627175257'),
('20190627175658'),
('20190627193330'),
('20190705153336'),
('20190705154140'),
('20200127195222'),
('20200127195547'),
('20200128151614'),
('20200306204604'),
('20200312011534'),
('20200415192636'),
('20200505001102'),
('20200506001147');


