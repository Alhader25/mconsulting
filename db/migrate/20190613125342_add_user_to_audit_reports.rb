class AddUserToAuditReports < ActiveRecord::Migration[5.2]
  def change
    add_reference :audit_reports, :user, foreign_key: true
  end
end
