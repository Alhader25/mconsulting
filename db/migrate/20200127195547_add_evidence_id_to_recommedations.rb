class AddEvidenceIdToRecommedations < ActiveRecord::Migration[5.2]
  def change
    add_reference :recommendations, :evidence, foreign_key: true
  end
end
