class CreateEvidences < ActiveRecord::Migration[5.2]
  def change
    create_table :evidences do |t|
      t.string :title
      t.text :text

      t.timestamps
    end
    add_reference :evidences, :audit_report, foreign_key: true
  end
end
