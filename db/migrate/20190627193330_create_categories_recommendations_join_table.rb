class CreateCategoriesRecommendationsJoinTable < ActiveRecord::Migration[5.2]
  def change
    create_join_table :categories, :recommendations do |t|
      t.index :category_id
      t.index :recommendation_id
    end
  end
end
