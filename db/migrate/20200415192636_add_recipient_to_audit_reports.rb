class AddRecipientToAuditReports < ActiveRecord::Migration[5.2]
  def change
    add_column :audit_reports, :recipient, :string
  end
end
