class CreateRecommendations < ActiveRecord::Migration[5.2]
  def change
    create_table :recommendations do |t|
      t.integer :year
      t.text :text
      t.text :title
      t.text :reason
      t.decimal :cost
      t.decimal :recovered
      t.text :justification
      t.text :response
      t.references :audit_report, foreign_key: true

      t.timestamps
    end
  end
end
