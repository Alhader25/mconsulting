class CreateMinistries < ActiveRecord::Migration[5.2]
  def change
    create_table :ministries do |t|
      t.string :name
      t.string :abbreviation

      t.timestamps
    end
    add_reference :organizations, :ministry, foreign_key: true
  end
end
