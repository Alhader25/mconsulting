class AddReportsToAuditReports < ActiveRecord::Migration[5.2]
  def change
    add_column :audit_reports, :reports, :json
  end
end
