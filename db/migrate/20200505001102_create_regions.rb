class CreateRegions < ActiveRecord::Migration[5.2]
  def change
    create_table :regions do |t|
      t.string :name

      t.timestamps
    end
    add_reference :organizations, :region, foreign_key: true
  end
end
