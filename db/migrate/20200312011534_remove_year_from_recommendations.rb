class RemoveYearFromRecommendations < ActiveRecord::Migration[5.2]
  def change
    remove_column :recommendations, :year
  end
end
