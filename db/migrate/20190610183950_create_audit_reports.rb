class CreateAuditReports < ActiveRecord::Migration[5.2]
  def change
    create_table :audit_reports do |t|
      t.integer :year
      t.text :title
      t.text :text
      t.integer :period

      t.timestamps
    end
  end
end
