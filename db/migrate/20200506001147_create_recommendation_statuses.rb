class CreateRecommendationStatuses < ActiveRecord::Migration[6.0]
  def change
    create_table :recommendation_statuses do |t|
      t.string :name

      t.timestamps
    end
    add_reference :recommendations, :recommendation_status, foreign_key: true
    add_foreign_key :categories_recommendations, :categories
    add_foreign_key :categories_recommendations, :recommendations
  end
end
