class AddSensitivityToAuditReports < ActiveRecord::Migration[5.2]
  #updated to use postgreSQL enum type: https://www.sitepoint.com/enumerated-types-with-activerecord-and-postgresql/
  def up
    execute <<-SQL
      CREATE TYPE sensitivity AS ENUM ('high', 'low');
    SQL
    add_column :audit_reports, :sensitivity, :sensitivity
  end

  def down
    remove_column :audit_reports, :sensitivity

    execute <<-SQL
      DROP TYPE sensitivity;
    SQL
  end
end
