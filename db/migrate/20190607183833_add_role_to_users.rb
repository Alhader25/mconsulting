class AddRoleToUsers < ActiveRecord::Migration[5.2]
  #updated to use postgreSQL enum type: https://www.sitepoint.com/enumerated-types-with-activerecord-and-postgresql/
  def up
    execute <<-SQL
      CREATE TYPE app_role AS ENUM ('admin', 'user', 'restricted_user');
    SQL
    add_column :users, :role, :app_role, index: true
  end

  def down
    remove_column :users, :role

    execute <<-SQL
      DROP TYPE app_role;
    SQL
  end
end
