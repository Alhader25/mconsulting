class AddStatusToRecommendations < ActiveRecord::Migration[5.2]
  #updated to use postgreSQL enum type: https://www.sitepoint.com/enumerated-types-with-activerecord-and-postgresql/
  def up
    execute <<-SQL
      CREATE TYPE recommendation_status AS ENUM ('complete', 'incomplete', 'ongoing', 'unknown');
    SQL
    add_column :recommendations, :status, :recommendation_status
  end

  def down
    remove_column :recommendations, :status

    execute <<-SQL
      DROP TYPE recommendation_status;
    SQL
  end
end
