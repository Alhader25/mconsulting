class AddAuditTypeToAuditReports < ActiveRecord::Migration[5.2]
  def change
    add_reference :audit_reports, :audit_type, foreign_key: true
  end
end
