class AddOrganizationToAuditReports < ActiveRecord::Migration[5.2]
  def change
    add_column :audit_reports, :control_organization_id, :integer, null: false
    add_index :audit_reports, :control_organization_id

    add_column :audit_reports, :verified_organization_id, :integer, null: false
    add_index :audit_reports, :verified_organization_id

    add_foreign_key :audit_reports, :organizations, column: :control_organization_id, primary_key: :id
    add_foreign_key :audit_reports, :organizations, column: :verified_organization_id, primary_key: :id
  end
end
