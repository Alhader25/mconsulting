class AddDatesToAuditReport < ActiveRecord::Migration[5.2]
  def change
    add_column :audit_reports, :arrival_date, :date
    add_column :audit_reports, :assignment_date, :date
    add_column :audit_reports, :received_date, :date
    add_column :audit_reports, :confirmation_date, :date
    add_column :audit_reports, :completion_date, :date
  end
end
