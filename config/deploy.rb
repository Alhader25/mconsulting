# config valid for current version and patch releases of Capistrano
lock "~> 3.14.1"

set :application, "audit-tracker"
set :repo_url, "git@bitbucket.org:Alhader25/mconsulting.git"
set :deploy_to, "/home/ubuntu/audit-tracker/#{fetch :application}"
#set :deploy_to, "/home/cgsp/audit-tracker/#{fetch :application}"
append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', '.bundle', 'public/system', 'public/uploads'

# Only keep the last 5 releases to save disk space
set :keep_releases, 5
set :keep_releases, 5
set :rbenv_path, '/home/ubuntu/.rbenv'
#set :rbenv_path, '/home/cgsp/.rbenv'
set :chruby_ruby, 'ruby-2.6.3'
# seed
