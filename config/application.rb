require_relative 'boot'
require 'csv'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module AuditTracker
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    #set default locale
    #https://guides.rubyonrails.org/i18n.html#configure-the-i18n-module
    config.i18n.default_locale = :fr

    # set locale load path to allow nesting
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    config.active_record.schema_format = :sql
    config.generators.javascript_engine = :js

    #config.web_console.whitelisted_ips = '172.18.0.1'
  end
end
