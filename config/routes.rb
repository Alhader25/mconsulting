Rails.application.routes.draw do
  resources :recommendation_statuses
  resources :regions
  devise_for :users
  devise_scope :user do
    get '/login' => 'devise/sessions#new'
    get '/logout' => 'devise/sessions#destroy'
  end
  resources :users, :controller => "users"

  resources :audit_reports do
    resources :recommendations, :evidences
    collection{post:import}
  end

  get '/sensitivities' => 'audit_reports#sensitivities'
  get '/recommendations' => 'recommendations#index'
  get '/annexe' => 'recommendations#annexe'
  get '/recommendations/export' => 'recommendations#export_csv', defaults: { format: 'csv'}

  resources :audit_types 
  resources :organizations do
    collection{post:import}
  end
  resources :categories do
    collection{post:import}
  end
  resources :ministries do
    collection{post:import}
  end

  get '/reports/ministries/control' => 'reports#control_ministries'
  get '/reports/ministries/verified' => 'reports#verified_ministries'
  get '/reports/organizations/control' => 'reports#control_organizations'
  get '/reports/organizations/verified' => 'reports#verified_organizations'
  get '/reports/categories/rootstatus' => 'reports#root_category_totals'
  get '/reports/categories/rootstatus/csv' => 'reports#root_category_totals_csv'
  get '/reports/categories/rootstatus/summary' => 'reports#root_category_totals_summary'
  get '/reports/recommendations/category' => 'reports#recommendation_categories'
  get '/reports/recommendations/category/csv' => 'reports#recommendation_categories_csv'
  get '/reports/recommendations/category/summary' => 'reports#recommendation_categories_summary'
  get '/reports/exports' => 'reports#exports'
  get '/reports/exports/all/csv' => 'reports#export_all_csv'
  #alhader
  get '/reports/rapport' => 'reports#rapport'
  get '/recommendations/rapport' => 'recommendations#rapport'
  root "audit_reports#index"
end


