FROM ruby:2.6.3
RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg main" > /etc/apt/sources.list.d/pgdg.list \
 && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - \
 && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
 && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
 && apt-get update -qq && apt-cache search postgresql | grep postgresql-client \
 && apt-get install -y postgresql-client-12 \
 && apt-get install yarn -y \
 && curl -sL https://deb.nodesource.com/setup_8.x | bash - \
 && apt install nodejs -y \
 && apt-get clean autoclean \
 && apt-get autoremove -y \
 && rm -rf \
    /var/lib/apt \
    /var/lib/dpkg \
    /var/lib/cache \
    /var/lib/log
RUN mkdir /audit-tracker
WORKDIR /audit-tracker
COPY Gemfile /audit-tracker/Gemfile
COPY Gemfile.lock /audit-tracker/Gemfile.lock
RUN bundle install
COPY . /audit-tracker

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]