namespace :users do
  #docker-compose run web rake users:seed_users
  desc "seed dummy users"
  task seed_users: :environment do
    cgsp = Organization.find_by({abbreviation: 'CGSP'})
    15.times do |index|
      ar = User.create!(email: Faker::Internet.email,
                    password: 'sngpcgsp',
                    role: ['admin', 'user'].sample,
                    confirmed_at: Faker::Date.between(from: 1.year.ago, to: Date.today),
                    organization_id: cgsp.id,
                    first_name: Faker::Name.first_name,
                    last_name: Faker::Name.last_name)
    end

    p "Created #{User.count} users"
  end

end
