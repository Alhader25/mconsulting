namespace :audit_reports do
  #docker-compose run web rake audit_reports:seed_audit_reports
  desc "create 50 dummy ARs"
  task seed_audit_reports: :environment do
    control_ids = Organization.control.ids
    verified_ids = Organization.verified.ids
    user_ids = User.ids
    50.times do |index|
      ar = AuditReport.create!(title: Faker::Hipster.sentence,
                    text: Faker::Hipster.paragraph,
                    year: Random.rand(2017...2020),
                    period: Random.rand(1...5),
                    arrival_date: Faker::Date.between(from: 1.year.ago, to: Date.today),
                    assignment_date: Faker::Date.between(from: 1.year.ago, to: Date.today),
                    received_date: Faker::Date.between(from: 1.year.ago, to: Date.today),
                    confirmation_date: Faker::Date.between(from: 1.year.ago, to: Date.today),
                    completion_date: Faker::Date.between(from: 1.year.ago, to: Date.today),
                    sensitivity: ['high', 'low'].sample,
                    control_organization_id: control_ids.sample,
                    verified_organization_id: verified_ids.sample,
                    audit_type_id: Random.rand(1...3),
                    user_id: user_ids.sample)
    end

    p "Created #{AuditReport.count} ARs"
  end

  #docker-compose run web rake audit_reports:seed_evidences
  desc "create 1 to 3 dummy evidences per AR"
  task seed_evidences: :environment do
    ars = AuditReport.all
    ars.each do |ar|
      Random.rand(1...4).times do |index|
        Evidence.create!(title: Faker::Hipster.sentence,
                      text: Faker::Hipster.paragraph,
                      audit_report_id: ar.id)
      end
    end

    p "Created #{Evidence.count} evidences"
  end

  #docker-compose run web rake audit_reports:seed_recommendations
  desc "create 1 to 8 dummy recommendations per evidence"
  task seed_recommendations: :environment do
    evidences = Evidence.all
    evidences.each do |evidence|
      Random.rand(1...9).times do |index|
        Recommendation.create!(title: Faker::Hipster.sentence,
                      text: Faker::Hipster.paragraph,
                      reason: Faker::Hipster.paragraph,
                      cost: Faker::Number.decimal(5),
                      recovered: Faker::Number.decimal(5),
                      justification: Faker::Hipster.paragraph,
                      response: Faker::Hipster.paragraph,
                      recommendation_status_id: Random.rand(1...5),
                      evidence_id: evidence.id,
                      audit_report_id: evidence.audit_report.id,
                      category_ids: Array.new(Random.rand(1...6)) { Random.rand(1...106) } )
      end
    end

    p "Created #{Recommendation.count} recs"
  end

end
